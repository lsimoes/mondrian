<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    protected $fillable = [
        'name', 'class_room_id', 'calendar_category_id', 'date', 'period', 'description'
    ];
}
