<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarCategory extends Model
{
    protected $fillable = [
        'name', 'color'
    ];
}
