<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Image;
use App\Banner;
use Storage;
use DB;
use File;

class BannerSchoolController extends Controller
{
    private $banner;

    public function __construct(Banner $banner)
    {
        $this->middleware('auth');
        $this->middleware('isadmin');
        
        $this->banner = $banner;
    }

    public function index()
    {
        $title = "Banner";

        $banners = DB::table('banners')->get();
                       
        return view('school.banner.index', compact('title','banners'));
    }

    public function incluir()
    {
        $title = "Banner";
                       
        return view('school.banner.create-edit', compact('title'));
    }

    public function insert(Request $request)
    {
        $filename = Uuid::generate();
        $image = $request->file('image');
        $extension = $request->file('image')->getClientOriginalExtension();

        $insert = $this->banner->create([
                    'name' => $request['name'],
                    'order' => $request['order'],
                    'image' => $filename.'.'.$extension,
                    'link' => $request['link']
                ]);

        $img = Image::make($image)->resize(1200, 1200, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->encode($extension, 80);

        Storage::disk('local')->put('public/banners/'.$filename.'.'.$extension, (string)$img);
        
        if($insert){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function delete(Request $request)
    {
        $banner = DB::table('banners')
                    ->where('id', $request['id'])
                        ->first();

        $delete = $this->banner
                        ->where('id', $request['id'])
                        ->delete();

        File::delete(storage_path('app/public/banners/'.$banner->image));

        if($delete){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function edit($id)
    {
        $title = "Banner";

        $banner = DB::table('banners')
                    ->where('id', $id)
                        ->first();
                       
        return view('school.banner.create-edit', compact('title','banner'));
    }

    public function update(Request $request)
    {
        $filename = Uuid::generate();
        $image = $request->file('image');
        $extension = $request->file('image')->getClientOriginalExtension();


        $banner = DB::table('banners')
                    ->where('id', $request['banner_id'])
                        ->first();
        File::delete(storage_path('app/public/banners/'.$banner->image));

        $update = $this->banner
                ->where('id', $request['banner_id'])
                ->update([
                    'name' => $request['name'],
                    'order' => $request['order'],
                    'image' => $filename.'.'.$extension,
                    'link' => $request['link']
                ]);  

        $img = Image::make($image)->resize(1200, 1200, function($constraint) {
                $constraint->aspectRatio();
                $constraint->upsize();
            })->encode($extension, 80);

        Storage::disk('local')->put('public/banners/'.$filename.'.'.$extension, (string)$img);
        
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }
}
