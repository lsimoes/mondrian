<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CalendarCategory;
use DB;

class CalendarCategorySchoolController extends Controller
{
	private $calendar_category;

	public function __construct(CalendarCategory $calendar_category)
    {
        $this->middleware('auth');
        $this->middleware('isadmin');
        
        $this->calendar_category = $calendar_category;
    }

    public function index()
    {
        $title = "Categorias";

        $calendar_categories = DB::table('calendar_categories')->get();
                       
        return view('school.calendar.category.index', compact('title','calendar_categories'));
    }

    public function adicionar()
    {
    	$title = "Categoria";
                       
        return view('school.calendar.category.create-edit', compact('title'));
    }

    public function insert(Request $request)
    {
        $insert = $this->calendar_category->create([
                    'name' => $request['name'],
                    'color' => $request['color'],
                ]);
        
        if($insert){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->calendar_category
                        ->where('id', $request['id'])
                        ->delete();
        if($delete){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function edit($id)
    {
    	$title = "Categoria";

    	$calendar_category = DB::table('calendar_categories')
    		->where('id', $id)
    		->first();
                       
        return view('school.calendar.category.create-edit', compact('title','calendar_category'));
    }

    public function update(Request $request)
    {
        $update = $this->calendar_category
                ->where('id', $request['calendar_category_id'])
                ->update([
                    'name' => $request['name'],
                    'color' => $request['color'],
                ]);        
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }
}
