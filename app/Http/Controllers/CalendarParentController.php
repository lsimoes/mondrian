<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;
use Auth;

class CalendarParentController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('isparent');
        $this->middleware('userstatus');
	}
	
    public function index()
    {
        $title = "Calendário";

        $calendar_categories = DB::table('calendar_categories')->get();
                       
        return view('parent.calendar.index', compact('title','calendar_categories'));
    }

    public function view($id)
    {
        $title = "Calendário";

        $calendar = DB::table('calendars')
            ->where('id', $id)
            ->first();
                       
        return view('parent.calendar.view', compact('title','calendar'));
    }


    public function load()
    {
        $classes = array();
        $students = DB::table('students')
                ->where('user_id', Auth::user()->id)
                ->get();

        array_push($classes, 1);

        foreach($students as $student)
        {
            array_push($classes, $student->class_room_id);
        }

        $calendars = DB::table('calendars')
            ->whereIn('class_room_id', $classes)
            ->get();

        $arrCalendar = array();

        foreach($calendars as $calendar)
        {
            $category = DB::table('calendar_categories')
                ->where('id', $calendar->calendar_category_id)
                ->first();

            $class_room = DB::table('class_rooms')
                ->where('id', $calendar->class_room_id)
                ->first();

            $arrCalendar[] = [
                'id' => $calendar->id,
                'title' => $calendar->name . ' - ' . $class_room->name,
                'date' => $calendar->date,
                'period' => $calendar->period,
                'url' => 'calendario/'.$calendar->id,
                'color' => $category->color,
                'textColor' => '#FFFFFF'
            ];
        }
        return Response::json($arrCalendar);
    }
}
