<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Response;
use Webpatser\Uuid\Uuid;
use App\Calendar;
use App\CalendarCategory;
use Storage;
use DB;

class CalendarSchoolController extends Controller
{
    
    private $calendar;

    public function __construct(CalendarCategory $calendar_category, Calendar $calendar)
    {
        $this->middleware('auth');
        $this->middleware('isadmin');
        
        $this->calendar_category = $calendar_category;
        $this->calendar = $calendar;
    }

    public function index()
    {
        $title = "Calendário";

        $calendar_categories = DB::table('calendar_categories')->get();
                       
        return view('school.calendar.index', compact('title','calendar_categories'));
    }

    public function incluir()
    {
        $title = "Calendário";

        $class_rooms = DB::table('class_rooms')->get();
        $calendar_categories = DB::table('calendar_categories')->get();
                       
        return view('school.calendar.create-edit', compact('title','class_rooms','calendar_categories'));
    }

    public function edit($id)
    {
        $title = "Calendário";

        $calendar = DB::table('calendars')
            ->where('id', $id)
            ->first();

        $class_rooms = DB::table('class_rooms')->get();
        $calendar_categories = DB::table('calendar_categories')->get();
                       
        return view('school.calendar.create-edit', compact('title','class_rooms','calendar_categories','calendar'));
    }

    

    public function insertCalendar(Request $request)
    {
        $insert = $this->calendar->create([
                    'name' => $request['name'],
                    'class_room_id' => $request['class_room_id'],
                    'calendar_category_id' => $request['calendar_category_id'],
                    'date' => $request['date'],
                    'period' => $request['period'],
                    'description' => $request['description'],
                ]);
        
        if($insert){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function update(Request $request)
    {
        $update = $this->calendar
                ->where('id', $request['calendar_id'])
                ->update([
                    'name' => $request['name'],
                    'class_room_id' => $request['class_room_id'],
                    'calendar_category_id' => $request['calendar_category_id'],
                    'date' => $request['date'],
                    'period' => $request['period'],
                    'description' => $request['description'],
                ]); 

        if($update){
            return response(200);
        } else {
            return response(400);
        }

    }

    public function delete(Request $request)
    {

        $delete = $this->calendar
                        ->where('id', $request['id'])
                        ->delete();


        if($delete){
            return response(200);
        } else {
            return response(400);
        }
    }

    

    public function load()
    {
        $calendars = DB::table('calendars')->get();

        $arrCalendar = array();

        foreach($calendars as $calendar)
        {
            $category = DB::table('calendar_categories')
                ->where('id', $calendar->calendar_category_id)
                ->first();

            $arrCalendar[] = [
                'id' => $calendar->id,
                'title' => $calendar->name,
                'date' => $calendar->date,
                'period' => $calendar->period,
                'url' => 'calendarios/editar/'.$calendar->id,
                'color' => $category->color,
                'textColor' => '#FFFFFF'
            ];
        }
        return Response::json($arrCalendar);
    }
}
