<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ClassRoom;
use DB;

class ClassRoomSchoolController extends Controller
{
    private $classroom;

    public function __construct(ClassRoom $classroom)
    {
        $this->middleware('auth');
        $this->middleware('isadmin');
        
        $this->classroom = $classroom;
    }

    public function index()
    {
        $title = "Turmas";

        $arrClassrooms = array();

        $classrooms = DB::table('class_rooms')
            ->where('id', '<>', 1) //todas
            ->get();

        foreach($classrooms as $classroom)
        {
            $students = DB::table('students')
                ->where('class_room_id', $classroom->id)
                ->get();

            $quantity = count($students);

            $arrClassrooms[] = [
                'id' => $classroom->id,
                'teacher' => $classroom->teacher,
                'name' => $classroom->name,
                'quantity' => $quantity
            ];
        }

                       
        return view('school.classroom.index', compact('title','arrClassrooms'));
    }

    public function incluir()
    {
        $title = "Turma";
      
        return view('school.classroom.create-edit', compact('title'));
    }

    public function insert(Request $request)
    {
        $insert = $this->classroom->create([
                    'teacher' => $request['teacher'],
                    'name' => $request['name']
                ]);
        
        if($insert){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function edit($id)
    {
        $title = "Turma";

        $class_room = DB::table('class_rooms')
            ->where('id', $id)
            ->first();
                       
        return view('school.classroom.create-edit', compact('title','class_room'));
    }

    public function update(Request $request)
    {
        $update = $this->classroom
                ->where('id', $request['class_room_id'])
                ->update([
                    'teacher' => $request['teacher'],
                    'name' => $request['name']
                ]);        
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->classroom
                        ->where('id', $request['id'])
                        ->delete();
        if($delete){
            return response(200);
        } else {
            return response(400);
        }
    }
}
