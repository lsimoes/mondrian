<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class EventParentController extends Controller
{
    public function index()
    {
        $title = "Eventos";

        $events = DB::table('events')
        	->where('status', '1')
            ->orderBy('id', 'DESC')
        	->get();
                       
        return view('parent.event.index', compact('title','events'));
    }

    public function event($id)
    {
        $title = "Evento";

        $event = DB::table('events')
        	->where('id', $id)
        	->first();
                       
        return view('parent.event.view', compact('title','event'));
    }
}
