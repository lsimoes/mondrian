<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Image;
use App\Event;
use Storage;
use DB;
use File;

class EventSchoolController extends Controller
{
    private $event;

    public function __construct(Event $event)
    {
        $this->middleware('auth');
        $this->middleware('isadmin');
        
        $this->event = $event;
    }

    public function index()
    {
        $title = "Eventos";

        $events = DB::table('events')->orderBy('id', 'DESC')->get();
                       
        return view('school.event.index', compact('title','events'));
    }

    public function incluir()
    {
        $title = "Evento";
                       
        return view('school.event.create-edit', compact('title'));
    }

    public function aprovar($id)
    {
        $title = "Aprovar";

        $event = DB::table('events')
                    ->where('id', $id)
                        ->first();
                       
        return view('school.event.moderate', compact('title','event'));
    }

    public function edit($id)
    {
        $title = "Evento";

        $event = DB::table('events')
                    ->where('id', $id)
                        ->first();
                       
        return view('school.event.create-edit', compact('title','event'));
    }


    public function insert(Request $request)
    {
        if($request['images'] != null)
        {
            $imagesName = ""; 
            foreach($request->file('images') as $image)
            {
                $filename = Uuid::generate();
                $extension = $image->getClientOriginalExtension();

                $img = Image::make($image)->orientate()->resize(800, 800, function($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->encode($extension, 80)->insert(storage_path('app/public/watermark.png'),'bottom-right', 20, 20);

                $img->save(storage_path('app/public/events/'.$filename.'.'.$extension, (string)$img));

                // Storage::disk('local')->put('public/events/'.$filename.'.'.$extension, (string)$img);
                $imagesName .= $filename.'.'.$extension .";";
            }
            $imagesName = substr_replace($imagesName, "", -1);
        }
        else
        {
            $imagesName = null;
        }

        $insert = $this->event->create([
                    'name' => $request['name'],
                    'date' => $request['date'],
                    'period' => $request['period'],
                    'description' => $request['description'],
                    'images' => $imagesName,
                    'videos' => $request['videos'],
                ]);

        if($insert){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function update(Request $request)
    {
        $event = DB::table('events')
                    ->where('id', $request['event_id'])
                        ->first();

        if($request['images'] != null)
        {
            $imagesName = ""; 
            foreach($request->file('images') as $image)
            {
                $filename = Uuid::generate();
                $extension = $image->getClientOriginalExtension();

                $img = Image::make($image)->orientate()->resize(800, 800, function($constraint) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    })->encode($extension, 80)->insert(storage_path('app/public/watermark.png'),'bottom-right', 20, 20);

                $img->save(storage_path('app/public/events/'.$filename.'.'.$extension, (string)$img));

                // Storage::disk('local')->put('public/events/'.$filename.'.'.$extension, (string)$img);
                $imagesName .= $filename.'.'.$extension .";";
            }
            $imagesName = substr_replace($imagesName, "", -1);
        }
        else
        {
            $imagesName = null;
        }

        if($imagesName == null)
        {
            $imagesName = $event->images;
        }
        else
        {
            $imagesName .= ';'.$event->images;
            // $imagesName = substr_replace($imagesName, "", -1);
        }

        $update = $this->event
                ->where('id', $request['event_id'])
                ->update([
                    'name' => $request['name'],
                    'date' => $request['date'],
                    'period' => $request['period'],
                    'description' => $request['description'],
                    'images' => $imagesName,
                    'videos' => $request['videos'],
                ]);        
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function delete(Request $request)
    {
        $event = DB::table('events')
                    ->where('id', $request['id'])
                        ->first();

        $images = explode(';', $event->images);

        foreach($images as $image)
        {
            echo $image . "<br>";
            File::delete(storage_path('app/public/events/'.$image));
        }

        $delete = $this->event
                        ->where('id', $request['id'])
                        ->delete();

        if($delete){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function deletePhoto(Request $request)
    {
        $event = DB::table('events')
                    ->where('id', $request['event_id'])
                        ->first();
        $imagesArr = array();               
        $images = explode(';', $event->images);

        foreach($images as $image)
        {
            array_push($imagesArr, $image);
        }

        $pos = array_search($request['image'], $imagesArr);
        unset($imagesArr[$pos]);

        $imagesFile = "";
        foreach($imagesArr as $image)
        {   
            $imagesFile .= $image.';';
        }

        $imagesName = substr_replace($imagesFile, "", -1);

        $update = $this->event
                ->where('id', $request['event_id'])
                ->update([
                    'images' => $imagesName,
                ]);    

        $deletePhoto = File::delete(storage_path('app/public/events/'.$request['image']));

        if($deletePhoto){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function publish(Request $request)
    {
        $update = $this->event
                ->where('id', $request['event_id'])
                ->update([
                    'status' => 1,
                ]);
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }
}
