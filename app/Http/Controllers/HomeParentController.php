<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class HomeParentController extends Controller
{

	public function __construct()
	{
		$this->middleware('auth');
        $this->middleware('isparent');
        $this->middleware('userstatus');
	}

    public function index()
    {
        $title = "Home";

        $banners = DB::table('banners')
            ->orderBy('order')
            ->get();

        $classes = array();
        $students = DB::table('students')
                ->where('user_id', Auth::user()->id)
                ->where('matriculation_date', date('Y'))
                ->get();

        array_push($classes, 1);

        foreach($students as $student)
        {
            array_push($classes, $student->class_room_id);
        }

        $notices = DB::table('notices')
            ->leftJoin('class_rooms', 'class_rooms.id', 'notices.class_room_id')
            ->whereIn('notices.class_room_id', $classes)
            //->where('students.user_id', Auth::user()->id)
            ->get([
                'notices.type as noticeType',
                'notices.id as noticeId',
                'notices.subject as noticeSubject',
                'class_rooms.name as classRoomName',
                'notices.date as noticeDate',
                'notices.aware as noticeAware'
            ]);

            $collection = collect($notices);

            $unique_data = $collection->unique()->values()->all(); 

            $notices = $unique_data;
                       
        return view('parent.home.index', compact('title','banners','notices'));
    }
}
