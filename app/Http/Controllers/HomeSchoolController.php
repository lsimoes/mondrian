<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class HomeSchoolController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('isadmin');
    }
    
    public function index()
    {
        $title = "Dashboard";

        $users = DB::table('users')
        	->where('status', null)
            ->where('isAdmin', null)
        	->get();

        $students = DB::table('students')
        	->where('status', '2')
        	->get();
                       
        return view('school.home.index', compact('title','users','students'));
    }
}
