<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Meeting;
use Mail;

class MeetingParentController extends Controller
{
    private $meeting;

    public function __construct(Meeting $meeting)
    {
        $this->middleware('auth');
        $this->middleware('isparent');
        $this->middleware('userstatus');

        $this->meeting = $meeting;
    }
    
    public function index()
    {
        $title = "Reuniões";

        $arrMeetings = array();
        $meetings = DB::table('meetings')
            ->leftJoin('students', 'students.class_room_id', 'meetings.class_room_id')
            ->leftJoin('class_rooms', 'class_rooms.id', 'meetings.class_room_id')
            ->where('students.user_id', Auth::user()->id)
            ->get([
                'students.id as studentId',
                'students.name_student as studentName',
                'meetings.id as meetingId',
                'meetings.confirmations as meetingConfirmations',
                'meetings.description as meetingDesc',
                'class_rooms.name as classRoomName',
                'class_rooms.teacher as classRoomTeacher',
                'meetings.dates as meetingDates',
                'meetings.hours as meetingHours',
                'meetings.month as meetingMonth',
                'meetings.class_room_id as meetingClassRoomId',
            ]);

        $collection = collect($meetings);

        $meetingsCorrect = $collection->unique('meetingMonth'); 

        //dd($meetingsCorrect);

        $arrMeetingConfirmations = array();

        foreach($meetings as $meeting)
        {

            if($meeting->meetingConfirmations != null)
            {
                
                    array_push($arrMeetingConfirmations, $meeting->meetingConfirmations);
                
            }
        }
        

        function searchConfirmation($studentId, $date, $meetingId, $arr)
        {
            
            if($arr != [])
            {
                foreach($arr as $confirmation)
                {
                    $confirms = explode(';', $confirmation);

                    foreach($confirms as $confirm)
                    {
                        $search = explode(',', $confirm);

                        if($search[3] == $studentId && $search[4] == $meetingId)
                        {
                            return $search[0] . ',' . $search[1] . ',' . $search[2] . ',' . $search[3] . ',' . $search[4];
                        }
                    }

                    
                }
            }
            //dd($studentId);
        }


        foreach($meetings as $meeting)
        {

            if($meeting->meetingMonth != null)
            {
                $dates = '';

                $meetingDates = DB::table('meetings')
                    ->where('dates', 'like', '%' . $meeting->meetingMonth . '%')
                    ->where('class_room_id', '=', $meeting->meetingClassRoomId)
                    ->get([
                        'meetings.dates as dates'
                    ]);

                    foreach($meetingDates as $meetingDate)
                    {
                        $dates .= $meetingDate->dates . ';';
                    }

                    $dates = substr($dates, 0, -1);

                    //dd($dates);
            }
            else
            {
                $dates = $meeting->meetingDates;    
            }
            

            $arrMeetings[] = [
                'studentId' => $meeting->studentId,
                'studentName' => $meeting->studentName,
                'meetingId' => $meeting->meetingId,
                'meetingTeacher' => $meeting->classRoomTeacher,
                'meetingDesc' => $meeting->meetingDesc,
                'classRoomName' => $meeting->classRoomName,
                'meetingDate' => $dates,
                'meetingMonth' => $meeting->meetingMonth . '|' . $meeting->studentId,
                'confirm' => searchConfirmation($meeting->studentId, $meeting->meetingDates, $meeting->meetingId, $arrMeetingConfirmations),
            ];


        }

        //dd($arrMeetingConfirmations);

        $collection = collect($arrMeetings);

        $meetingsCorrect = $collection->unique('meetingMonth'); 

        $arrMeetings = $meetingsCorrect;

        //dd($meetingsCorrect);

        //dd($arrMeetings);


                       
        return view('parent.meeting.index', compact('title','arrMeetings'));
    }

    public function schedule($id, $studentId)
    {
        $title = "Agendar";

        $meeting = DB::table('meetings')
            ->leftJoin('students', 'students.class_room_id', 'meetings.class_room_id')
            ->leftJoin('class_rooms', 'class_rooms.id', 'meetings.class_room_id')
            ->where('students.id', $studentId)
            ->where('meetings.id', $id)
            ->first([
                'students.id as studentId',
                'students.name_student as studentName',
                'meetings.id as meetingId',
                'meetings.description as meetingDescription',
                'class_rooms.teacher as meetingTeacher',
                'meetings.confirmations as meetingConfirmations',
                'class_rooms.name as classRoomName',
                'meetings.dates as meetingDates',
                'meetings.hours as meetingHours',
                'meetings.month as meetingMonth',
                'meetings.class_room_id as meetingClassRoomId',
            ]);

        if($meeting->meetingMonth != null)
        {
            $dates = '';
            $hours = '';

            $meetingDates = DB::table('meetings')
                ->where('dates', 'like', '%' . $meeting->meetingMonth . '%')
                ->where('class_room_id', '=', $meeting->meetingClassRoomId)
                ->get([
                    'meetings.dates as dates'
                ]);

                foreach($meetingDates as $meetingDate)
                {
                    $dates .= $meetingDate->dates . ';';
                }

                $dates = substr($dates, 0, -1);

                //dd($dates);
        }
        else
        {
            $dates = $meeting->meetingDates;  
        }

        //dd($dates);



        $arrHours = array();
        $arrHoursConfirmed = array();

        if($meeting->meetingConfirmations != null && $meeting->meetingConfirmations != '')
        {
            foreach(explode(';', $meeting->meetingConfirmations) as $confirmations)
            {
                $confirmation = explode(',', $confirmations);
                array_push($arrHoursConfirmed, $confirmation[1] . ';' .$confirmation[2]);
            }
        }

        // dd($arrHoursConfirmed);

        foreach(explode(';', $dates) as $date)
        {

            $meetingHours = DB::table('meetings')
                ->where('dates', 'like', '%' . $date . '%')
                ->where('class_room_id', '=', $meeting->meetingClassRoomId)
                ->get([
                    'meetings.hours as hours'
                ]);

            foreach($meetingHours as $meetingHour)
            {
                foreach(explode(';', $meetingHour->hours) as $hour)
                {
                    array_push($arrHours, $date . ';' . $hour);
                }
            }

            
        }

        

        $hoursDisponible = array_diff($arrHours, $arrHoursConfirmed);


        $arrHoursDisponible = array();

        foreach($hoursDisponible as $hourDisponible)
        {
            $datehour = explode(';', $hourDisponible);
            $arrHoursDisponible[] = [
                'date' => $datehour[0],
                'hour' => $datehour[1]
            ];
        }

        $arrHoursSorted = array();
        foreach ($arrHoursDisponible as $element) {
            $timestamp = strtotime($element['date']);
            $date = date("Y-m-d", $timestamp); //truncate hours:minutes:seconds
            if ( ! isSet($arrHoursSorted[$date]) ) { //first entry of that day
                $arrHoursSorted[$date] = array($element);
            } else { //just push current element onto existing array
                $arrHoursSorted[$date][] = $element;
            }
        }

        //dd($arrHoursSorted);

        // dd($arrHoursSorted['2019-04-16']);
        return view('parent.meeting.schedule', compact('title','meeting','arrHoursSorted','dates'));
    }

    public function confirm(Request $request)
    {

        $meeting = DB::table('meetings')
                    ->where('id', $request['meeting_id'])
                        ->first();

        $idStudent = explode(',', $request['datehour']);

        $confirmations = '';

        $theConfirmations = $meeting->confirmations;               
                       
        if($meeting->confirmations != null && $meeting->confirmations != '')
        {

            foreach(explode(';', $meeting->confirmations) as $confirmation)
            {

                $confirm = explode(',', $confirmation);



                if($confirm[0] == Auth::user()->id && $confirm[3] == $idStudent[3])
                {

                    $remove = $confirm[0].','.$confirm[1].','.$confirm[2].','.$confirm[3].','.$confirm[4];

                    $clean = str_replace($remove, '', $theConfirmations);

                    $confirmations = $clean .';'. $request['datehour'];

                    $confirmations = str_replace(';;', ';', $confirmations);

                    $confirmations = ltrim($confirmations, ';');


                    // dd($confirmations);
                }
            }
        }
        else
        {
            $confirmations = $request['datehour'];

        }



        if($confirmations == '')
        {
            $confirmations = $theConfirmations .';'. $request['datehour'];
        }


        $user = DB::table('users')
                    ->where('id', Auth::user()->id)
                        ->first();

        $student = DB::table('students')
                    ->where('id', $idStudent[3])
                        ->first();


        $content = 'Olá, o responsável <strong>' . $user->name . '</strong> confirmou uma reunião para o aluno <strong>' . $student->name_student . '</strong> para ' . $idStudent[1] . ' as ' . $idStudent[2] . '.';

        Mail::send(['html' => 'emails.send'], ['title' => 'Reunião confirmada!', 'content' => $content], function ($message)
        {
            $message->subject('Reunião confirmada!');

            $message->from('suporte@escolamondrian.com.br', 'Sistema Mondrian');

            $message->to('escolamondrian@escolamondrian.com.br');

        });
  
        $update = $this->meeting
                ->where('id', $request['meeting_id'])
                ->update([
                    'confirmations' => $confirmations,
                ]);  
        
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }
}
