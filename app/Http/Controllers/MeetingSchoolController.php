<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\Meeting;
use Storage;
use DB;
use Response;

class MeetingSchoolController extends Controller
{
    private $meeting;

    public function __construct(Meeting $meeting)
    {
        $this->middleware('auth');
        $this->middleware('isadmin');
        
        $this->meeting = $meeting;
    }

    public function index()
    {
        $title = "Reuniões";

        $arrConfirms = array();
        $arrMeetings = array();

        $meetings = DB::table('meetings')
            ->where(function($q) {
                  $q->where('confirmations', '!=', '');
              })
            ->get();
         
        foreach($meetings as $meeting)
        {
            foreach(explode(';', $meeting->confirmations) as $confirmation)
            {
                array_push($arrConfirms, $meeting->id.','.$confirmation);
            }
        }
        // dd($arrConfirms);
        foreach($arrConfirms as $confirms)
        {
            $confirmation = explode(',', $confirms);
            
            $meeting = DB::table('meetings')
                ->where('id', $confirmation[0])
                ->first();

            $classroom = DB::table('class_rooms')
                ->where('id', $meeting->class_room_id)
                ->first();

            $user = DB::table('users')
                ->where('id', $confirmation[1])
                ->first();
            
            $date = $confirmation[2];

            $hour = $confirmation[3];

            $student_id = $confirmation[4];

            $student = DB::table('students')
                ->where('id', $student_id)
                ->first();

            if($student)
            {
                $arrMeetings[] = [
                    'userId' => $student->user_id,
                    'studentId' => $student->id,
                    'meetingId' => $meeting->id,
                    'meetingTeacher' => $classroom->teacher,
                    'meetingClassRoom' => $classroom->name,
                    'meetingStudent' => $student->name_student,
                    'userName' => $user->name,
                    'meetingDate' => $date,
                    'meetingHour' => $hour
                ];
            }
        }

                       
        return view('school.meeting.index', compact('title','arrMeetings'));
    }

    public function incluir()
    {
        $title = "Reunião";

        $classrooms = DB::table('class_rooms')->get();
                       
        return view('school.meeting.create-edit', compact('title','classrooms'));
    }

    public function edit($id)
    {
        $title = "Reunião";

        $meeting = DB::table('meetings')
            ->where('id', $id)
            ->first();

        $classrooms = DB::table('class_rooms')->get();
                       
        return view('school.meeting.create-edit', compact('title','classrooms','meeting'));
    }

    public function calendario()
    {
        $title = "Calendário Reunião";
                       
        return view('school.meeting.calendar.index', compact('title'));
    }

    public function insert(Request $request)
    {

        $dates = "";
        $hours = "";
        $months = array();

        foreach($request['dates'] as $date)
        {
            $dates .= $date . ';';

            $month = explode('-', $date);

            array_push($months, $month[0].'-'.$month[1]);
        }

        function array_is_unique($array) {
           return array_unique($array) == $array;
        }

        if(count($months) > 1)
        {
            if(array_is_unique($months))
            {
                return "date-error";
                exit();
            }
        }

        foreach($request['hours'] as $hour)
        {
            $hours .= $hour . ';';
        }

        $dates = substr_replace($dates, "", -1);
        $hours = substr_replace($hours, "", -1);

        $insert = $this->meeting->create([
                    'class_room_id' => $request['class_room_id'],
                    'description' => $request['description'],
                    'dates' => $dates,
                    'hours' => $hours,
                    'month' => $months[0],
                ]);
        
        if($insert){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function update(Request $request)
    {

        $dates = "";
        $hours = "";

        

        foreach($request['dates'] as $date)
        {
            $dates .= $date . ';';
        }

        foreach($request['hours'] as $hour)
        {
            $hours .= $hour . ';';
        }

        $dates = substr_replace($dates, "", -1);
        $hours = substr_replace($hours, "", -1);


        $update = $this->meeting
                ->where('id', $request['meeting_id'])
                ->update([
                    'class_room_id' => $request['class_room_id'],
                    'description' => $request['description'],
                    'dates' => $dates,
                    'hours' => $hours,
                ]); 
        
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->meeting
                        ->where('id', $request['id'])
                        ->delete();


        if($delete){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function deleteConfirmation(Request $request)
    {
        $meeting = explode(',', $request['confirmation']);

        $meetings = DB::table('meetings')
                ->where('id', $meeting[4])
                ->first();

        $clean = str_replace($request['confirmation'], '', $meetings->confirmations);

        $confirmations = str_replace(';;', ';', $clean);

        $confirmations = ltrim($confirmations, ';');

        $update = $this->meeting
                ->where('id', $meeting[4])
                ->update([
                    'confirmations' => $confirmations,
                ]);

    }

    public function load()
    {
        $meetings = DB::table('meetings')->get();

        $arrMeetings = array();

        foreach($meetings as $meeting)
        {
            $classroom = DB::table('class_rooms')
                ->where('id', $meeting->class_room_id)
                ->first();

            foreach(explode(';', $meeting->dates) as $date)
            {
                foreach(explode(';', $meeting->hours) as $hour)
                {
                    //dd($meeting->hours);
                    $arrMeetings[] = [
                        'title' => $classroom->teacher . ' - ' . $classroom->name,
                        'url' => 'editar/'.$meeting->id,
                        'start' => $date . 'T' . $hour .':00',
                        'textColor' => '#FFFFFF',

                    ];
                }
            }
        }

        // dd($arrMeetings);

        return Response::json($arrMeetings);
    }
}
