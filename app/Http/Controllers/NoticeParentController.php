<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Notice;

class NoticeParentController extends Controller
{
    private $notice;

    public function __construct(Notice $notice)
    {
        $this->middleware('auth');
        $this->middleware('isparent');
        
        $this->notice = $notice;
    }

    public function notices()
    {
        return redirect('pais/home');
    }

    public function notice($id)
    {
        $title = "Aviso";

        $notice = DB::table('notices')
            ->leftJoin('students', 'students.class_room_id', 'notices.class_room_id')
            ->leftJoin('class_rooms', 'class_rooms.id', 'notices.class_room_id')
            //->where('students.user_id', Auth::user()->id)
            ->where('notices.id', $id)
            // ->orderBy('notices.id', 'DESC')
            ->first([
                'students.name_student as studentName',
                'notices.type as noticeType',
                'notices.id as noticeId',
                'notices.subject as noticeSubject',
                'notices.notice as noticeNotice',
                'notices.number as noticeNumber',
                'class_rooms.name as classRoomName',
                'notices.date as noticeDate',
                'notices.aware as noticeAware'
            ]);
                    //dd($notice);     
        return view('parent.notice.view', compact('title','notice'));
    }

    public function aware(Request $request)
    {
        $notice = DB::table('notices')
            ->where('id', $request['notice_id'])
            ->first();

        $awares = Auth::user()->id .';'. $notice->aware;
        $awares = substr_replace($awares, "", -1);

        $update = $this->notice
                ->where('id', $request['notice_id'])
                ->update([
                    'aware' => $awares
                ]);        
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }
}
