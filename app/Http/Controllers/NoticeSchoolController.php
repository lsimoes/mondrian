<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Image;
use App\Notice;
use Storage;
use DB;


class NoticeSchoolController extends Controller
{
    private $notice;

    public function __construct(Notice $notice)
    {
        $this->middleware('auth');
        $this->middleware('isadmin');
        
        $this->notice = $notice;
    }

    public function index()
    {
        $title = "Avisos";

        $noticesArr = array();
        

        $notices = DB::table('notices')
                ->join('class_rooms', 'class_rooms.id', 'notices.class_room_id')
                ->orderBy('notices.id', 'DESC') 
                ->get([
                    'notices.id as noticeId',
                    'notices.subject as noticeSubject',
                    'notices.number as noticeNumber',
                    'notices.date as noticeDate',
                    'notices.aware as noticeAware',
                    'class_rooms.name as classRoomName',
                ]);

        foreach($notices as $notice)
        {
            $usersArr = array();

            if($notice->noticeAware != null)
            {
                foreach(explode(';', $notice->noticeAware) as $aware)
                {
                    $user = DB::table('users')
                        ->where('id', $aware)
                        ->first();
                    if($user)
                    {
                        array_push($usersArr, $user->name);
                    }
                    
                }
            }


            $noticesArr[] = [
                'noticeId' => $notice->noticeId,
                'noticeSubject' => $notice->noticeSubject,
                'noticeNumber' => $notice->noticeNumber,
                'noticeDate' => $notice->noticeDate,
                'classRoomName' => $notice->classRoomName,
                'awares' => $usersArr
            ];
        }
                       
        return view('school.notice.index', compact('title','noticesArr'));
    }

    public function incluir()
    {
        $title = "Aviso";

        $classrooms = DB::table('class_rooms')->get();
                       
        return view('school.notice.create-edit', compact('title','classrooms'));
    }

    public function insert(Request $request)
    {   
        if($request['class_room_id'][0] == 1)
        {
            $insert = $this->notice->create([
                    'subject' => $request['subject'],
                    'date' => $request['date'],
                    'type' => $request['type'],
                    'number' => $request['number'],
                    'notice' => $request['notice'],
                    'class_room_id' => 1,
                ]);
        }
        else
        {
            foreach($request['class_room_id'] as $class_room_id)
            {
                $insert = $this->notice->create([
                        'subject' => $request['subject'],
                        'date' => $request['date'],
                        'type' => $request['type'],
                        'number' => $request['number'],
                        'notice' => $request['notice'],
                        'class_room_id' => $class_room_id,
                    ]);
            }
        }
        
        if($insert){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function update(Request $request)
    {
        if($request['class_room_id'][0] == 1)
        {
            $update = $this->notice
                    ->where('id', $request['notice_id'])
                    ->update([
                        'subject' => $request['subject'],
                        'date' => $request['date'],
                        'type' => $request['type'],
                        'number' => $request['number'],
                        'notice' => $request['notice'],
                        'class_room_id' => 1,
                    ]);
        }
        else
        {
            foreach($request['class_room_id'] as $class_room_id)
            {
                $update = $this->notice
                    ->where('id', $request['notice_id'])
                    ->update([
                        'subject' => $request['subject'],
                        'date' => $request['date'],
                        'type' => $request['type'],
                        'number' => $request['number'],
                        'notice' => $request['notice'],
                        'class_room_id' => $class_room_id,
                    ]);
            }
        }       
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function edit($id)
    {
        $title = "Aviso";

        $classrooms = DB::table('class_rooms')->get();

        $notice = DB::table('notices')
            ->where('id', $id)
            ->first();
                       
        return view('school.notice.create-edit', compact('title','classrooms','notice'));
    }

    public function delete(Request $request)
    {
        $delete = $this->notice
                        ->where('id', $request['id'])
                        ->delete();
        if($delete){
            return response(200);
        } else {
            return response(400);
        }
    }
}
