<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Image;
use App\Student;
use Storage;
use DB;
use Auth;
use Mail;

class StudentParentController extends Controller
{
    private $student;

    public function __construct(Student $student)
    {
        $this->middleware('auth');
        $this->middleware('isparent', ['except' => ['step1', 'step2', 'step3', 'step4', 'step5']]);

        $this->student = $student;
    }
    
    public function index()
    {
        $title = "Alunos";

        $students = DB::table('students')
                ->where('user_id', Auth::user()->id)
                ->leftJoin('class_rooms', 'class_rooms.id', 'students.class_room_id')
                ->get([
                    'students.id as studentId',
                    'students.name_student as studentName',
                    'students.photo as studentPhoto',
                    'students.status as studentStatus',
                    'students.matriculation_date as studentMatriculationDate',
                    'students.rinumber as studentRiNumber',
                    'students.updated_at as studentUpdatedAt',
                    'class_rooms.name as classRoomName',
                    ]);      

        return view('parent.student.index', compact('title','students'));
    }

    public function insert()
    {
        $title = "Aluno";
                       
        return view('parent.student.create-edit', compact('title'));
    }

    public function edit($id)
    {
        $title = "Aluno";

        $student = DB::table('students')
            ->where('id', $id)
            ->first();
                       
        return view('parent.student.create-edit', compact('title','student'));
    }

    public function step1(Request $request)
    {

        if($request['photo'] != null)
        {
            $filename = Uuid::generate();
            $image = $request->file('photo');
            $extension = $request->file('photo')->getClientOriginalExtension();
            $photo = $filename.'.'.$extension;
        }
        else
        {
            $photo = $request['photo_user'];
        }

        $brothers = "";

        if($request['brothername1'] != null && $request['brotherage1'] != null)
        {
            $brothers .= $request['brothername1'] . ',' . $request['brotherage1'] . ';';
        }

        if($request['brothername2'] != null && $request['brotherage2'] != null)
        {
            $brothers .= $request['brothername2'] . ',' . $request['brotherage2'] . ';';
        }

        if($request['brothername3'] != null && $request['brotherage3'] != null)
        {
            $brothers .= $request['brothername3'] . ',' . $request['brotherage3'] . ';';
        }

        if($request['brothername4'] != null && $request['brotherage4'] != null)
        {
            $brothers .= $request['brothername4'] . ',' . $request['brotherage4'] . ';';
        }
        
        $meals = "";

        foreach($request['meals'] as $meal)
        {
            $meals .= $meal . ';';
        }

        $meals = substr_replace($meals, "", -1);
        $brothers = substr_replace($brothers, "", -1);

        if(Auth::user()->isAdmin == 0)
        {
            $user_id = Auth::user()->id;
        }
        else
        {
            $user_id = null;
        }

        if($request['student_id'] != '')
        {
            $insert = $this->student
                    ->where('id', $request['student_id'])
                    ->update([
                        'name_student' => $request['name_student'],
                        'date_birth_student' => $request['date_birth_student'],
                        'race' => $request['race'],
                        'certificate' => $request['certificate'],
                        'rg' => $request['rg'],
                        'photo' => $photo,
                        'father_power' => $request['father_power'],
                        'catch_child' => $request['catch_child'],
                        'period' => $request['period'],
                        'meals' => $meals,
                        'brothers' => $brothers,
                    ]);
        }
        else
        {

            $insert = $this->student->create([
                    'name_student' => $request['name_student'],
                    'date_birth_student' => $request['date_birth_student'],
                    'race' => $request['race'],
                    'certificate' => $request['certificate'],
                    'rg' => $request['rg'],
                    'photo' => $photo,
                    'father_power' => $request['father_power'],
                    'catch_child' => $request['catch_child'],
                    'period' => $request['period'],
                    'meals' => $meals,
                    'brothers' => $brothers,
                    'user_id' => $user_id,
                    'status' => '0',
                ]);


        }

        
        

        if($request['photo'] != null)
        {
            $img = Image::make($image)->resize(800, 800, function($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                })->encode($extension, 80);

            Storage::disk('local')->put('public/students/'.$filename.'.'.$extension, (string)$img);
        }

        
        if($insert){
            if($request['place_url'] == 'editar')
            {
                $content = 'Olá, os Dados do Aluno do aluno <strong>' . $request['name_student'] . '</strong> foram alterados pelo seu responsável.';

                Mail::send(['html' => 'emails.send'], ['title' => 'Dados de aluno alterado!', 'content' => $content], function ($message)
                {
                    $message->subject('Dados de aluno alterado!');

                    $message->from('suporte@escolamondrian.com.br', 'Sistema Mondrian');

                    $message->to('escolamondrian@escolamondrian.com.br');

                });
            }

            if($request['student_id'] == '')
            {
                return $insert->id;
            }
            else
            {
                return $request['student_id'];
            }
        } else {
            return response(400);
        }

    }

    public function step2(Request $request)
    {
        $insert = $this->student
                    ->where('id', $request['student_id'])
                    ->update([
                        'allergy' => $request['allergy'],
                        'food_restriction' => $request['food_restriction'],
                        'medicines_often' => $request['medicines_often'],
                        'blood_group' => $request['blood_group'],
                        'medical_agreement' => $request['medical_agreement'],
                        'fever_case' => $request['fever_case'],
                        'health_observations' => $request['health_observations']
                    ]);

        if($insert){
            if($request['place_url'] == 'editar')
            {
                $student = DB::table('students')
                    ->where('id', $request['student_id'])
                    ->first();

                $content = 'Olá, as Informações Médicas do aluno <strong>' . $student->name_student . '</strong> foram alteradas pelo seu responsável.';

                Mail::send(['html' => 'emails.send'], ['title' => 'Dados de aluno alterado!', 'content' => $content], function ($message)
                {
                    $message->subject('Dados de aluno alterado!');

                    $message->from('suporte@escolamondrian.com.br', 'Sistema Mondrian');

                    $message->to('escolamondrian@escolamondrian.com.br');

                });
            }

            return response(200);
        } else {
            return response(400);
        }
    }

    public function step3(Request $request)
    {
        $insert = $this->student
                    ->where('id', $request['student_id'])
                    ->update([
                        'cep_student' => $request['cep_student'],
                        'address_student' => $request['address_student'],
                        'number_student' => $request['number_student'],
                        'complement_student' => $request['complement_student'],
                        'neighborhood_student' => $request['neighborhood_student'],
                        'city_student' => $request['city_student'],
                        'state_student' => $request['state_student']
                    ]);

        if($insert){
            if($request['place_url'] == 'editar')
            {
                $student = DB::table('students')
                    ->where('id', $request['student_id'])
                    ->first();

                $content = 'Olá, o Endereço do aluno <strong>' . $student->name_student . '</strong> foi alterado pelo seu responsável.';

                Mail::send(['html' => 'emails.send'], ['title' => 'Dados de aluno alterado!', 'content' => $content], function ($message)
                {
                    $message->subject('Dados de aluno alterado!');

                    $message->from('suporte@escolamondrian.com.br', 'Sistema Mondrian');

                    $message->to('escolamondrian@escolamondrian.com.br');

                });
            }

            return response(200);
        } else {
            return response(400);
        }
    }

    public function step4(Request $request)
    {
        $insert = $this->student
                    ->where('id', $request['student_id'])
                    ->update([
                        'name_dad' => $request['name_dad'],
                        'date_birth_dad' => $request['date_birth_dad'],
                        'cpf_dad' => $request['cpf_dad'],
                        'email_dad' => $request['email_dad'],
                        'cell_dad' => $request['cell_dad'],
                        'job_dad' => $request['job_dad'],
                        'comm_tel_dad' => $request['comm_tel_dad'],
                        'comm_cep_dad' => $request['comm_cep_dad'],
                        'comm_address_dad' => $request['comm_address_dad'],
                        'comm_number_dad' => $request['comm_number_dad'],
                        'comm_complement_dad' => $request['comm_complement_dad'],
                        'comm_neighborhood_dad' => $request['comm_neighborhood_dad'],
                        'comm_city_dad' => $request['comm_city_dad'],
                        'comm_state_dad' => $request['comm_state_dad'],

                        'name_mom' => $request['name_mom'],
                        'date_birth_mom' => $request['date_birth_mom'],
                        'cpf_mom' => $request['cpf_mom'],
                        'email_mom' => $request['email_mom'],
                        'cell_mom' => $request['cell_mom'],
                        'job_mom' => $request['job_mom'],
                        'comm_tel_mom' => $request['comm_tel_mom'],
                        'comm_cep_mom' => $request['comm_cep_mom'],
                        'comm_address_mom' => $request['comm_address_mom'],
                        'comm_number_mom' => $request['comm_number_mom'],
                        'comm_complement_mom' => $request['comm_complement_mom'],
                        'comm_neighborhood_mom' => $request['comm_neighborhood_mom'],
                        'comm_city_mom' => $request['comm_city_mom'],
                        'comm_state_mom' => $request['comm_state_mom'],
                     ]);

        
        

        if($insert){
            if($request['place_url'] == 'editar')
            {
                $student = DB::table('students')
                    ->where('id', $request['student_id'])
                    ->first();

                $content = 'Olá, os Dados dos Pais do aluno <strong>' . $student->name_student . '</strong> foram alterados pelo seu responsável.';

                Mail::send(['html' => 'emails.send'], ['title' => 'Dados de aluno alterado!', 'content' => $content], function ($message)
                {
                    $message->subject('Dados de aluno alterado!');

                    $message->from('suporte@escolamondrian.com.br', 'Sistema Mondrian');

                    $message->to('escolamondrian@escolamondrian.com.br');

                });
            }

            return response(200);
        } else {
            return response(400);
        }
    }

    public function step5(Request $request)
    {
        //se o status for 1 nao altera o status pra 2
        $student = DB::table('students')
                ->where('id', $request['student_id'])
                ->first();

                //dd($student);

        if($student->status == "1")
        {
            $status = '1';
        }
        else
        {
            $status = '2';
        }

        $insert = $this->student
                    ->where('id', $request['student_id'])
                    ->update([
                        'image_authorization' => $request['image_authorization'],
                        'status' => $status
                    ]);

        if($request['place_url'] == 'editar')
        {
            $student = DB::table('students')
                ->where('id', $request['student_id'])
                ->first();

            $content = 'Olá, a Autorização de Imagem do aluno <strong>' . $student->name_student . '</strong> foi alterada pelo seu responsável.';

            Mail::send(['html' => 'emails.send'], ['title' => 'Dados de aluno alterado!', 'content' => $content], function ($message)
            {
                $message->subject('Dados de aluno alterado!');

                $message->from('suporte@escolamondrian.com.br', 'Sistema Mondrian');

                $message->to('escolamondrian@escolamondrian.com.br');

            });
        }
        else
        {
            $content = 'Olá, o sistema recebeu o cadastro do aluno <strong>' . $student->name_student . '</strong> e aguarda por matrícula.';

            Mail::send(['html' => 'emails.send'], ['title' => 'Novo aluno cadastrado!', 'content' => $content], function ($message)
            {
                $message->subject('Novo aluno cadastrado!');

                $message->from('suporte@escolamondrian.com.br', 'Sistema Mondrian');

                $message->to('escolamondrian@escolamondrian.com.br');

            });
        }

        if($insert){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->student
                        ->where('id', $request['student_id'])
                        ->delete();
        if($delete){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function rematch(Request $request)
    {


        $student = Student::find($request['student_id']);

        $newStudent = $student->replicate();
        $newStudent->status = null;
        $newStudent->matriculation_date = null;
        $newStudent->class_room_id = null;
        if($newStudent->save())
        {
            return response(200);
        }
        else
        {
            return response(400);
        }
    }

}
