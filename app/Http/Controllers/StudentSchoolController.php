<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use Image;
use App\Student;
use Storage;
use DB;
use Auth;
use Mail;

class StudentSchoolController extends Controller
{
    private $student;

    public function __construct(Student $student)
    {
        $this->middleware('auth');
        $this->middleware('isadmin', ['except' => ['step1', 'step2', 'step3', 'step4', 'step5']]);
        
        $this->student = $student;
    }

    public function index()
    {
        $title = "Alunos";

        $students = DB::table('students')
                ->leftJoin('class_rooms', 'class_rooms.id', 'students.class_room_id')
                ->leftJoin('users', 'users.id', 'students.user_id')
                ->where('students.status', '1')
                ->orWhere('students.status', '2')
                ->orWhere('students.status', null)
                ->get([
                    'students.id as studentId',
                    'students.name_student as studentName',
                    'users.name as userName',
                    'users.email as userEmail',
                    'students.status as studentStatus',
                    'students.matriculation_date as studentMatriculationDate',
                    'students.updated_at as studentUpdatedAt',
                    'class_rooms.name as classRoomName',
                    ]); 
                       
        return view('school.student.index', compact('title','students'));
    }

    public function edit($id)
    {
        $title = "Aluno";

        $class_rooms = DB::table('class_rooms')
            ->where('id', '<>', 1) //todas
            ->get();

        $student = DB::table('students')
                ->where('id', $id)
                ->first();

        $parent = DB::table('users')
                ->where('id', $student->user_id)
                ->first();
                       
        return view('school.student.create-edit', compact('title','class_rooms','student','parent'));
    }

    public function print($id)
    {
        $title = "Aluno";

        

        $student = DB::table('students')
                ->where('id', $id)
                ->first();

        $class_room = DB::table('class_rooms')
            ->where('id', '=', $student->class_room_id) //
            ->first();

        $parent = DB::table('users')
                ->where('id', $student->user_id)
                ->first();

        $brothers = explode(';', $student->brothers);
        $meals = explode(';', $student->meals);
                       
        return view('school.student.print', compact('title','class_room','student','parent','brothers','meals'));
    }

    public function update(Request $request)
    {
        $update = $this->student
                ->where('id', $request['student_id'])
                ->update([
                    'rinumber' => $request['rinumber'],
                    'class_room_id' => $request['class_room_id'],
                    'matriculation_date' => $request['matriculation_date'],
                    'status' => 1,
                ]);

        $student = DB::table('students')
                ->where('id', $request['student_id'])
                ->first();


        $user = DB::table('users')
                ->where('id', $student->user_id)
                ->first();

        $classroom = DB::table('class_rooms')
                ->where('id', $request['class_room_id'])
                ->first();

        $this->email = $user->email;   

        $content = 'Olá '. $user->name .', o aluno <strong>' . $student->name_student . '</strong> foi matriculado com o RI ' . $request['rinumber'] . ', ano letivo ' . $request['matriculation_date'] . ', turma ' . $classroom->name . '.';

        Mail::send(['html' => 'emails.send'], ['title' => 'Aluno matriculado!', 'content' => $content], function ($message)
        {
            $message->subject('Aluno matriculado!');

            $message->from('suporte@escolamondrian.com.br', 'Sistema Mondrian');

            $message->to($this->email);

        });

        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function cancel(Request $request)
    {
        $update = $this->student
                ->where('id', $request['student_id'])
                ->update([
                    'rinumber' => null,
                    'class_room_id' => null,
                    'matriculation_date' => null,
                    'status' => null,
                ]);
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->student
                        ->where('id', $request['student_id'])
                        ->delete();
        if($delete){
            return response(200);
        } else {
            return response(400);
        }
    }

    
}
