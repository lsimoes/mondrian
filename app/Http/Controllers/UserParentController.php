<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\User;
use Illuminate\Support\Facades\Hash;

class UserParentController extends Controller
{
	private $user;

	public function __construct(User $user)
    {
        $this->middleware('auth');
        $this->middleware('isparent');
        
        $this->user = $user;
    }

    public function index()
    {
        $title = "Usuário";

        $user = DB::table('users')
        	->where('id', Auth::user()->id)
        	->first();
                       
        return view('parent.user.create-edit', compact('title','user'));
    }

    public function update(Request $request)
    {
        if(trim($request['password']))
        {
            $update = $this->user
                    ->where('id', $request['user_id'])
                    ->update([
                        'name' => $request['name'],
                        'email' => $request['email'],
                        'password' => Hash::make($request['password']),
                    ]);
        }
        else
        {
            $update = $this->user
                    ->where('id', $request['user_id'])
                    ->update([
                        'name' => $request['name'],
                        'email' => $request['email'],
                    ]);
        }
        
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }
}
