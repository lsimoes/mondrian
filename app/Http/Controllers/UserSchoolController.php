<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Webpatser\Uuid\Uuid;
use App\User;
use Storage;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Validator;

class UserSchoolController extends Controller
{
    private $user;

    public function __construct(User $user)
    {
        $this->middleware('auth');
        $this->middleware('isadmin');
        
        $this->user = $user;
    }

    public function index()
    {
        $title = "Usuários";

        $users = DB::table('users')
        ->where('status', '1')
        ->get();
                       
        return view('school.user.index', compact('title','users'));
    }

    public function incluir()
    {
        $title = "Usuário";
                       
        return view('school.user.create-edit', compact('title'));
    }

    public function insert(Request $request)
    {
        $insert = $this->user->create([
                    'name' => $request['name'],
                    'email' => $request['email'],
                    'password' => Hash::make($request['password']),
                    'isAdmin' => $request['isAdmin'],
                    'status' => '1',
                ]);
        
        if($insert){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function edit($id)
    {
        $title = "Usuário";

        $user = DB::table('users')
                ->where('id', $id)
                ->first();
                       
        return view('school.user.create-edit', compact('title','user'));
    }

    public function update(Request $request)
    {
        if(empty($request['isAdmin']))
        {
            $isAdmin = null;
        }
        else
        {
            $isAdmin = 1;
        }

        if(trim($request['password']))
        {
            $update = $this->user
                    ->where('id', $request['user_id'])
                    ->update([
                        'name' => $request['name'],
                        'email' => $request['email'],
                        'password' => Hash::make($request['password']),
                        'isAdmin' => $isAdmin,
                        'status' => '1',
                    ]);
        }
        else
        {
            $update = $this->user
                    ->where('id', $request['user_id'])
                    ->update([
                        'name' => $request['name'],
                        'email' => $request['email'],
                        'isAdmin' => $isAdmin,
                        'status' => '1',
                    ]);
        }
        
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function delete(Request $request)
    {
        $delete = $this->user
                        ->where('id', $request['id'])
                        ->delete();
        if($delete){
            return response(200);
        } else {
            return response(400);
        }
    }

    public function approve(Request $request)
    {
        $update = $this->user
                    ->where('id', $request['id'])
                    ->update([
                        'status' => '1'
                    ]);
        if($update){
            return response(200);
        } else {
            return response(400);
        }
    }
}
