<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsParent
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->isAdmin == 1)
        {
            abort(403, 'Acesso negado.');
        }

        return $next($request);
    }
}
