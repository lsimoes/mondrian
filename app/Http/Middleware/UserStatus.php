<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class UserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->status == null)
        {
            //abort(403, 'No momento você não tem permissão de acessar esta área.');
            return redirect('pais/alunos');
        }

        return $next($request);
    }
}
