<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Meeting extends Model
{
    protected $fillable = [
        'teacher_id', 'class_room_id', 'dates', 'hours','description','confirmations','month'
    ];
}
