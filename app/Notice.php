<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{
    protected $fillable = [
        'subject', 'date','type','number','notice','class_room_id'
    ];
}
