<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $fillable = [
        'name_student', 'rinumber', 'date_birth_student', 'race', 'certificate', 'rg', 'phone_student','photo','father_power','catch_child','period','meals','brothers','allergy','food_restriction','medicines_often','blood_group','medical_agreement','fever_case','health_observations','cep_student','address_student','number_student','complement_student','neighborhood_student','city_student','state_student','name_dad','date_birth_dad','cpf_dad','email_dad','cell_dad','job_dad','comm_tel_dad','comm_cep_dad','comm_address_dad','comm_number_dad','comm_complement_dad','comm_neighborhood_dad','comm_city_dad','comm_state_dad','name_mom','date_birth_mom','cpf_mom','email_mom','cell_mom','job_mom','comm_tel_mom','comm_cep_mom','comm_address_mom','comm_number_mom','comm_complement_mom','comm_neighborhood_mom','comm_city_mom','comm_state_mom','image_authorization','status', 'matriculation_date','user_id','class_room_id'
    ];
}
