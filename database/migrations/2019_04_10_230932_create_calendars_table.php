<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255);
            $table->integer('class_room_id')->unsigned();
            $table->foreign('class_room_id')
                    ->references('id')->on('class_rooms');
            $table->integer('calendar_category_id')->unsigned();
            $table->foreign('calendar_category_id')
                    ->references('id')->on('calendar_categories');
            $table->date('date');
            $table->char('period');
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars');
    }
}
