<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_student', 255);
            $table->string('rinumber')->nullable();
            $table->char('schooling')->nullable();
            $table->date('date_birth_student');

            $table->string('race');
            $table->string('certificate');
            $table->string('rg');

            $table->string('photo');
            $table->string('father_power');
            $table->string('catch_child');
            $table->char('period');
            $table->char('meals');
            $table->text('brothers')->nullable();
            $table->string('allergy')->nullable();
            $table->string('food_restriction')->nullable();
            $table->string('medicines_often')->nullable();
            $table->string('blood_group')->nullable();
            $table->string('medical_agreement')->nullable();
            $table->string('fever_case')->nullable();
            $table->text('health_observations')->nullable();
            $table->string('cep_student')->nullable();
            $table->string('address_student')->nullable();
            $table->string('number_student')->nullable();
            $table->string('complement_student')->nullable();
            $table->string('neighborhood_student')->nullable();
            $table->string('city_student')->nullable();
            $table->string('state_student')->nullable();

            $table->string('name_dad')->nullable();
            $table->string('date_birth_dad')->nullable();
            $table->string('cpf_dad')->nullable();
            $table->string('email_dad')->nullable();
            $table->string('cell_dad')->nullable();
            $table->string('job_dad')->nullable();
            $table->string('comm_tel_dad')->nullable();
            $table->string('comm_cep_dad')->nullable();
            $table->string('comm_address_dad')->nullable();
            $table->string('comm_number_dad')->nullable();
            $table->string('comm_complement_dad')->nullable();
            $table->string('comm_neighborhood_dad')->nullable();
            $table->string('comm_city_dad')->nullable();
            $table->string('comm_state_dad')->nullable();

            $table->string('name_mom')->nullable();
            $table->string('date_birth_mom')->nullable();
            $table->string('cpf_mom')->nullable();
            $table->string('email_mom')->nullable();
            $table->string('cell_mom')->nullable();
            $table->string('job_mom')->nullable();
            $table->string('comm_tel_mom')->nullable();
            $table->string('comm_cep_mom')->nullable();
            $table->string('comm_address_mom')->nullable();
            $table->string('comm_number_mom')->nullable();
            $table->string('comm_complement_mom')->nullable();
            $table->string('comm_neighborhood_mom')->nullable();
            $table->string('comm_city_mom')->nullable();
            $table->string('comm_state_mom')->nullable();

            $table->char('image_authorization')->nullable();

            $table->char('status')->nullable();

            $table->year('matriculation_date')->nullable();
            

            $table->integer('class_room_id')->unsigned()->nullable();
            $table->foreign('class_room_id')
                    ->references('id')->on('class_rooms');

            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')
                    ->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
