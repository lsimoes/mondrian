<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notices', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject', 255);
            $table->date('date');
            $table->char('type');
            $table->string('number', 255);
            $table->text('notice');
            $table->integer('class_room_id')->unsigned();
            $table->foreign('class_room_id')
                    ->references('id')->on('class_rooms');
            $table->text('aware')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notices');
    }
}
