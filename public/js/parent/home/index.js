$(document).ready(function() {
    $('.table').dataTable( {
            "language": {
                "url": "/js/dist/datatables/Portuguese-Brasil.json"
            },
            columnDefs: [
		       { type: 'date-euro', targets: 3 }
		     ],

            "order": [[ 3, "desc" ]]
        });
} );