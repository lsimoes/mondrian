$(document).ready(function() {

    $("#formSchedule").submit(function(e){
        e.preventDefault();
        
        $(".confirm").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".confirm").attr("disabled", true);
        
        $.ajax({
            url : "/api/parent/confirm/meeting",
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : {
                datehour : $('.datehour:checked').val(),
                meeting_id : $('#meeting_id').val(),
                },
        })
        .done(function(msg){
            if(msg == 200)
            {
                $(location).attr('href', '/pais/reunioes');
            }
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".confirm").html("Confirmar");
            $(".confirm").attr("disabled", false);  
        }); 
    });

} );