$(document).ready(function() {
    $('body').on('click','.aware', function(e){
        e.preventDefault();
        
        $(".aware").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".aware").attr("disabled", true);
        
        $.ajax({
            url : "/api/parent/aware/notice",
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : {
                notice_id : $('#notice_id').val(),
                },
        })
        .done(function(msg){
            $(location).attr('href', '/pais/home');
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false);  
        }); 
    });
} );