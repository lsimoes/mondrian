$( document ).ready(function() {

    var query = window.location + '';
    var split = query.split('/');

    
    $("#formStudentStep1").submit(function(e){
        e.preventDefault();
        
        $(".btnNew1").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".btnNew1").attr("disabled", true);
        
        var formData = new FormData(this);
        
        $.ajax({
            url : "/api/parent/insert/student/step1",
            type : 'post',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : formData,
        })
        .done(function(msg){
            $("#medicas-tab").attr('class', 'nav-link');
            
            $('.student_id').val(msg);

            if($('.place_url').val() == 'editar')
            {
                if(split[3] == 'escola')
                {
                    $(location).attr('href', '/escola/alunos');
                }
                else
                {
                    $(location).attr('href', '/pais/alunos');
                }
            }

            $('#myTab a[href="#medicas"]').tab('show');

            $(".btnNew1").html("Próximo >");
            $(".btnNew1").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".btnNew1").html("Próximo >");
            $(".btnNew1").attr("disabled", false);  
        }); 
    });

    $("#formStudentStep2").submit(function(e){
        e.preventDefault();
        
        $(".btnNew2").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".btnNew2").attr("disabled", true);
        
        var formData = new FormData(this);
        
        $.ajax({
            url : "/api/parent/insert/student/step2",
            type : 'post',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : formData,
        })
        .done(function(msg){
            $("#endereco-tab").attr('class', 'nav-link');
            

            if($('.place_url').val() == 'editar')
            {
                if(split[3] == 'escola')
                {
                    $(location).attr('href', '/escola/alunos');
                }
                else
                {
                    $(location).attr('href', '/pais/alunos');
                }
            }

            $('#myTab a[href="#endereco"]').tab('show');

            $(".btnNew2").html("Próximo >");
            $(".btnNew2").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".btnNew2").html("Próximo >");
            $(".btnNew2").attr("disabled", false);  
        }); 
    });

    $("#formStudentStep3").submit(function(e){
        e.preventDefault();
        
        $(".btnNew3").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".btnNew3").attr("disabled", true);
        
        var formData = new FormData(this);
        
        $.ajax({
            url : "/api/parent/insert/student/step3",
            type : 'post',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : formData,
        })
        .done(function(msg){
            $("#pais-tab").attr('class', 'nav-link');
            

            if($('.place_url').val() == 'editar')
            {
                if(split[3] == 'escola')
                {
                    $(location).attr('href', '/escola/alunos');
                }
                else
                {
                    $(location).attr('href', '/pais/alunos');
                }
            }

            $('#myTab a[href="#pais"]').tab('show');

            $(".btnNew3").html("Próximo >");
            $(".btnNew3").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".btnNew3").html("Próximo >");
            $(".btnNew3").attr("disabled", false);  
        }); 
    });

    $("#formStudentStep4").submit(function(e){
        e.preventDefault();
        
        $(".btnNew4").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".btnNew4").attr("disabled", true);
        
        var formData = new FormData(this);
        
        $.ajax({
            url : "/api/parent/insert/student/step4",
            type : 'post',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : formData,
        })
        .done(function(msg){
            $("#imagem-tab").attr('class', 'nav-link');
            

            if($('.place_url').val() == 'editar')
            {
                if(split[3] == 'escola')
                {
                    $(location).attr('href', '/escola/alunos');
                }
                else
                {
                    $(location).attr('href', '/pais/alunos');
                }
            }

            $('#myTab a[href="#imagem"]').tab('show');

            $(".btnNew4").html("Próximo >");
            $(".btnNew4").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".btnNew4").html("Próximo >");
            $(".btnNew4").attr("disabled", false);  
        }); 
    });

    $("#formStudentStep5").submit(function(e){
        e.preventDefault();
        
        $(".btnNew5").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".btnNew5").attr("disabled", true);
        
        var formData = new FormData(this);
        
        $.ajax({
            url : "/api/parent/insert/student/step5",
            type : 'post',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : formData,
        })
        .done(function(msg){
            if(split[3] == 'escola')
            {
                $(location).attr('href', '/escola/alunos');
            }
            else
            {
                $(location).attr('href', '/pais/alunos');
            }
            
            $(".btnNew5").html("Finalizar");
            $(".btnNew5").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".btnNew5").html("Finalizar");
            $(".btnNew5").attr("disabled", false);  
        }); 
    });



    
    //Quando o campo cep perde o foco.
    $("#cep_student").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#address_student").val("...");
                $("#neighborhood_student").val("...");
                $("#city_student").val("...");
                $("#state_student").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#address_student").val(dados.logradouro);
                        $("#neighborhood_student").val(dados.bairro);
                        $("#city_student").val(dados.localidade);
                        $("#state_student").val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                alert("Formato de CEP inválido.");
            }
        } //end if.
    });

    $("#comm_cep_dad").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#comm_address_dad").val("...");
                $("#comm_neighborhood_dad").val("...");
                $("#comm_city_dad").val("...");
                $("#comm_state_dad").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#comm_address_dad").val(dados.logradouro);
                        $("#comm_neighborhood_dad").val(dados.bairro);
                        $("#comm_city_dad").val(dados.localidade);
                        $("#comm_state_dad").val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                alert("Formato de CEP inválido.");
            }
        } //end if.
    });

    $("#comm_cep_mom").blur(function() {

        //Nova variável "cep" somente com dígitos.
        var cep = $(this).val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if(validacep.test(cep)) {

                //Preenche os campos com "..." enquanto consulta webservice.
                $("#comm_address_mom").val("...");
                $("#comm_neighborhood_mom").val("...");
                $("#comm_city_mom").val("...");
                $("#comm_state_mom").val("...");

                //Consulta o webservice viacep.com.br/
                $.getJSON("https://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#comm_address_mom").val(dados.logradouro);
                        $("#comm_neighborhood_mom").val(dados.bairro);
                        $("#comm_city_mom").val(dados.localidade);
                        $("#comm_state_mom").val(dados.uf);
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                //cep é inválido.
                alert("Formato de CEP inválido.");
            }
        } //end if.
    });
});  