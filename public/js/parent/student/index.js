$(document).ready(function() {
    $('.table').dataTable( {
            "language": {
                "url": "/js/dist/datatables/Portuguese-Brasil.json"
            }
        });

    var id;

    $('body').on('click','.delete', function(e){
        e.preventDefault();
        id = $(this).data('id');
        $('.modal-body').html('Deseja exluir este aluno?');
        $('.modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button><button type="button" class="btn btn-danger confirm-delete">Sim</button>');
        $('#modal').modal('toggle');
    });

    $('body').on('click','.confirm-delete', function(e){
        e.preventDefault();
        
        $(".confirm-delete").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".confirm-delete").attr("disabled", true);
        
        $.ajax({
            url : "/api/parent/delete/student",
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : {
                student_id : id,
                },
        })
        .done(function(msg){
            $(location).attr('href', '/pais/alunos');
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false);  
        }); 
    });

    $('body').on('click','.rematch', function(e){
        e.preventDefault();
        id = $(this).data('id');
        $('.modal-body').html('<strong>Confira todos os dados de cadastro do aluno no botão EDITAR antes de fazer a rematricula!</strong> <br><br> Está ciente que as informações estão corretas e deseja rematricular este aluno?');
        $('.modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button><button type="button" class="btn btn-danger confirm-rematch">Sim</button>');
        $('#modal').modal('toggle');
    });

    $('body').on('click','.confirm-rematch', function(e){
        e.preventDefault();
        
        $(".confirm-delete").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".confirm-delete").attr("disabled", true);
        
        $.ajax({
            url : "/api/parent/rematch/student",
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : {
                student_id : id,
                },
        })
        .done(function(msg){
            $(location).attr('href', '/pais/alunos');
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false);  
        }); 
    });

    $('body').on('click','.newstudent', function(e){
        e.preventDefault();
        id = $(this).data('id');
        $('.modal-body').html('<strong>Esta opção é apenas para novos alunos!</strong> <br><br>Se o aluno já estiver cursando utilize o botão <strong>REMATRICULAR</strong>. <br><br> Deseja matricular um novo aluno? ');
        $('.modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button><button type="button" class="btn btn-danger confirm-newstudent">Sim</button>');
        $('#modal').modal('toggle');
    });

    $('body').on('click','.confirm-newstudent', function(e){
        e.preventDefault();
        
        $(".confirm-delete").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".confirm-delete").attr("disabled", true);
        
        $(location).attr('href', '/pais/alunos/matricular');
    });
} );