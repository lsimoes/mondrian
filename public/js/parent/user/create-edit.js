$( document ).ready(function() {
    
    $("#formUser").submit(function(e){
        e.preventDefault();
        
        $("#btnNew").html('<div class="spinner-border text-primary" role="status"></div>');
        $("#btnNew").attr("disabled", true);
        
        var formData = new FormData(this);

        
        $.ajax({
            url : "/api/parent/update/user",
            type : 'post',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : formData,
        })
        .done(function(msg){
            
            if(msg == 200)
            {
                $(location).attr('href', '/pais/home');
            }
        })
        .fail(function(jqXHR, textStatus, msg){
            $("#btnNew").html("Salvar");
            $("#btnNew").attr("disabled", false);  
        }); 
    });
});  