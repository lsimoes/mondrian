$( document ).ready(function() {
    
    $("#formCalendarCategory").submit(function(e){
        e.preventDefault();
        
        $("#btnNew").html('<div class="spinner-border text-primary" role="status"></div>');
        $("#btnNew").attr("disabled", true);
        
        var formData = new FormData(this);

        if($('#calendar_category_id').val())
        {
            url = "/api/school/update/calendar/category";
        }
        else
        {
            url = "/api/school/insert/calendar/category";
        }
        
        
        $.ajax({
            url : url,
            type : 'post',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : formData,
        })
        .done(function(msg){
            if(msg == 200)
            {
                $(location).attr('href', '/escola/calendarios/categorias');
            }
        })
        .fail(function(jqXHR, textStatus, msg){
            $("#btnNew").html("Salvar");
            $("#btnNew").attr("disabled", false);  
        }); 
    });
});  