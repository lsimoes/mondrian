$(document).ready(function() {
    var calendarEl = document.getElementById('calendario');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: [ 'dayGrid' ],
        locale: 'pt-br',
        events: {
          url: '/api/school/calendar/load',
          method: 'GET',
          failure: function() {
            alert('there was an error while fetching events!');
          },
          
        },

        eventClick: function(info) {
            
          },

    });

    calendar.render();
} );