$(document).ready(function() {
    $('.table').dataTable( {
            "language": {
                "url": "/js/dist/datatables/Portuguese-Brasil.json"
            }
        });

    var id;

    $('body').on('click','.delete', function(e){
        e.preventDefault();
        id = $(this).data('id');
        $('.modal-body').html('Deseja excluir esta turma?');
        $('.modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button><button type="button" class="btn btn-danger confirm-delete">Sim</button>');
        $('#modal').modal('toggle');
    });

    $('body').on('click','.confirm-delete', function(e){
        e.preventDefault();
        $(".confirm-delete").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".confirm-delete").attr("disabled", true);
        
        $.ajax({
            url : "/api/school/delete/classroom",
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : {
                id : id,
                },
        })
        .done(function(msg){
            location.reload();
            $(".confirm-delete").html("Sim");
            $(".confirm-cancel").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
        	$('.modal-body').html('Você não pode excluir esta turma pois possui outros itens vinculados a ela.');
        $('.modal-footer').html('');
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false);  
        }); 
    });
} );