$( document ).ready(function() {
    
    $("#formEvent").submit(function(e){
        e.preventDefault();
        
        $("#btnNew").html('<div class="spinner-border text-primary" role="status"></div>');
        $("#btnNew").attr("disabled", true);
        
        var formData = new FormData(this);

        if($('#event_id').val())
        {
            url = "/api/school/update/event";
        }
        else
        {
            url = "/api/school/insert/event";
        }
        
        $.ajax({
            url : url,
            type : 'post',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : formData,
        })
        .done(function(msg){
            
            if(msg == 200)
            {
                $(location).attr('href', '/escola/eventos');
            }
        })
        .fail(function(jqXHR, textStatus, msg){
            $("#btnNew").html("Salvar");
            $("#btnNew").attr("disabled", false);  
        }); 
    });
});  