$(document).ready(function() {
    var image;
    var event_id = $('#event_id').val();

    $('body').on('click','.delete', function(e){
        e.preventDefault();
        image = $(this).data('image');
        $('.modal-body').html('Deseja excluir esta foto?');
        $('.modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button><button type="button" class="btn btn-danger confirm-delete">Sim</button>');
        $('#modal').modal('toggle');
    });

    $('body').on('click','.confirm-delete', function(e){
        e.preventDefault();
        $(".confirm-delete").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".confirm-delete").attr("disabled", true);
        
        $.ajax({
            url : "/api/school/delete/event/photo",
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : {
                image : image,
                event_id : event_id,
                },
        })
        .done(function(msg){
            location.reload();
            $(".confirm-delete").html("Sim");
            $(".confirm-cancel").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false);  
        }); 
    });

    $('body').on('click','.publish', function(e){
        e.preventDefault();
        $(".publish").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".publish").attr("disabled", true);
        
        $.ajax({
            url : "/api/school/publish/event",
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : {
                event_id : event_id,
                },
        })
        .done(function(msg){
            $(location).attr('href', '/escola/eventos');
            $(".publish").html("Publicar");
            $(".publish").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".confirm-delete").html("Publicar");
            $(".confirm-delete").attr("disabled", false);  
        }); 
    });
} );