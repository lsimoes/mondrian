$(document).ready(function() {
    var calendarEl = document.getElementById('calendario');

    var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: [ 'dayGrid' ],
        locale: 'pt-br',

        events: {
          url: '/api/school/load/meeting',
          method: 'GET',
          failure: function() {
            alert('there was an error while fetching events!');
          },
          
        },
        displayEventEnd: true,
timeFormat: 'h:mma',
          

    });

    calendar.render();
} );