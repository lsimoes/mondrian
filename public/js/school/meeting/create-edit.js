$(document).ready(function() {
    $('body').on('click','.addDate', function(){
    	$(this).parent().parent().append('\n\
    		<div class="col-md-12">\n\
    			<div class="form-group col-md-5">\n\
			    	<input type="date" class="form-control" name="dates[]" id="date" required>\n\
			    </div>\n\
			    <div class="form-group col-md-1">\n\
			    	<a class="btn btn-danger delDate"><span class="oi oi-minus"></span></a>\n\
			    </div></div>');
    });

    $('body').on('click','.delDate', function(){
    	$(this).parent().parent().remove();
    });

    $('body').on('click','.addHour', function(){
    	$(this).parent().parent().append('\n\
    		<div class="col-md-12">\n\
    			<div class="form-group col-md-5">\n\
				    	<input type="time" class="form-control" name="hours[]" id="hours"> \n\
				    </div>\n\
				    <div class="form-group col-md-1">\n\
				    	<a class="btn btn-danger delHour"><span class="oi oi-minus"></span></a>\n\
				    </div></div>');
    });

    $('body').on('click','.delHour', function(){
    	$(this).parent().parent().remove();
    });

    $("#formMeeting").submit(function(e){
        e.preventDefault();
        
        $("#btnNew").html('<div class="spinner-border text-primary" role="status"></div>');
        $("#btnNew").attr("disabled", true);
        
        var formData = new FormData(this);

        if($('#meeting_id').val())
        {
            url = "/api/school/update/meeting";
        }
        else
        {
            url = "/api/school/insert/meeting";
        }
        
        $.ajax({
            url : url,
            type : 'post',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : formData,
        })
        .done(function(msg){
            
            if(msg == 200)
            {
                $(location).attr('href', '/escola/reunioes/calendario');
            }
            else if(msg == 'date-error')
            {
                alert('Selecione apenas datas dentro do mesmo mês.');
            }
        })
        .fail(function(jqXHR, textStatus, msg){
            $("#btnNew").html("Incluir");
            $("#btnNew").attr("disabled", false);  
        }); 
    });

    var id;

    $('body').on('click','.delete', function(e){
        e.preventDefault();
        id = $(this).data('id');
        $('.modal-body').html('Deseja excluir esta reunião?');
        $('.modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button><button type="button" class="btn btn-danger confirm-delete">Sim</button>');
        $('#modal').modal('toggle');
    });

    $('body').on('click','.confirm-delete', function(e){
        e.preventDefault();
        $(".confirm-delete").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".confirm-delete").attr("disabled", true);
        
        $.ajax({
            url : "/api/school/delete/meeting",
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : {
                id : id,
                },
        })
        .done(function(msg){
            $(location).attr('href', '/escola/reunioes/calendario')
            $(".confirm-delete").html("Sim");
            $(".confirm-cancel").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false);  
        }); 
    });
});