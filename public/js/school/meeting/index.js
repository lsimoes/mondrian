$(document).ready(function() {
    $('.table').dataTable( {
            "language": {
                "url": "/js/dist/datatables/Portuguese-Brasil.json"
            }
        });

    $('body').on('click','.delete', function(e){
        e.preventDefault();
        confirmation = $(this).data('confirmation');
        $('.modal-body').html('Deseja excluir esta confirmação?');
        $('.modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button><button type="button" class="btn btn-danger confirm-delete">Sim</button>');
        $('#modal').modal('toggle');
    });

    $('body').on('click','.confirm-delete', function(e){
        e.preventDefault();
        $(".confirm-delete").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".confirm-delete").attr("disabled", true);
        
        $.ajax({
            url : "/api/school/delete/meeting/confirmation",
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : {
                confirmation : confirmation,
                },
        })
        .done(function(msg){
            location.reload();
            $(".confirm-delete").html("Sim");
            $(".confirm-cancel").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
        	$('.modal-body').html('');
        $('.modal-footer').html('');
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false);  
        }); 
    });
} );