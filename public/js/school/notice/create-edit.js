$( document ).ready(function() {

    $('.summernote').summernote({
          height: 500,   //set editable area's height
          codemirror: { // codemirror options
            theme: 'monokai'
          },
          callbacks: {
              onImageUpload: function(files) {
                sendFile(files[0]);
              }
          }
        });
    
    $("#formNotice").submit(function(e){
        e.preventDefault();
        
        $("#btnNew").html('<div class="spinner-border text-primary" role="status"></div>');
        $("#btnNew").attr("disabled", true);
        
        var formData = new FormData(this);
        var url;

        if($('#notice_id').val())
        {
            url = "/api/school/update/notice";
        }
        else
        {
            url = "/api/school/insert/notice";
        }
        
        $.ajax({
            url : url,
            type : 'post',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : formData,
        })
        .done(function(msg){
            
            if(msg == 200)
            {
                $(location).attr('href', '/escola/avisos');
            }
        })
        .fail(function(jqXHR, textStatus, msg){
            $("#btnNew").html("Incluir");
            $("#btnNew").attr("disabled", false);  
        }); 
    });
});  