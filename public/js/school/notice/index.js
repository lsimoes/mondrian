$(document).ready(function() {
    $('.table').dataTable( {

            "language": {
                "url": "/js/dist/datatables/Portuguese-Brasil.json"
            },

            columnDefs: [
               { type: 'date-euro', targets: 3 }
             ],

            "order": [[ 3, "desc" ]]
        });

    var id;

    $('body').on('click','.delete', function(e){
        e.preventDefault();
        id = $(this).data('id');
        $('.modal-body').html('Deseja excluir este aviso?');
        $('.modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button><button type="button" class="btn btn-danger confirm-delete">Sim</button>');
        $('#modal').modal('toggle');
    });

    $('body').on('click','.confirm-delete', function(e){
        e.preventDefault();
        $(".confirm-delete").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".confirm-delete").attr("disabled", true);
        
        $.ajax({
            url : "/api/school/delete/notice",
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : {
                id : id,
                },
        })
        .done(function(msg){
            location.reload();
            $(".confirm-delete").html("Sim");
            $(".confirm-cancel").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false);  
        }); 
    });

    $('body').on('click','.awares', function(e){
        e.preventDefault();
        awares = $(this).data('awares');
        $('.modal-body').html(awares);
        $('#modal').modal('toggle');
    });
} );