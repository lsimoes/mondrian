$( document ).ready(function() {

	$("#formMatriculation").submit(function(e){
        e.preventDefault();
        
        $(".btnSave").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".btnSave").attr("disabled", true);
        
        var formData = new FormData(this);
        
        $.ajax({
            url : "/api/school/update/student",
            type : 'post',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : formData,
        })
        .done(function(msg){
        	location.reload();

            $(".btnSave").html("Matricular");
            $(".btnSave").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".btnSave").html("Matricular");
            $(".btnSave").attr("disabled", false);  
        }); 
    });

    $('body').on('click','.cancel', function(e){
        e.preventDefault();
        $('.modal-body').html('Deseja cancelar a matrícula deste aluno?');
        $('.modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button><button type="button" class="btn btn-danger confirm-cancel">Sim</button>');
        $('#modal').modal('toggle');
    });

    $('body').on('click','.confirm-cancel', function(e){
        e.preventDefault();
        
        $(".confirm-cancel").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".confirm-cancel").attr("disabled", true);
        
        $.ajax({
            url : "/api/school/cancel/student",
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : {
                student_id : $('#student_id').val(),
                },
        })
        .done(function(msg){
            location.reload();
            $(".confirm-cancel").html("Sim");
            $(".confirm-cancel").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".confirm-cancel").html("Sim");
            $(".confirm-cancel").attr("disabled", false);  
        }); 
    });

    $('body').on('click','.delete', function(e){
        e.preventDefault();
        $('.modal-body').html('Deseja exluir este aluno?');
        $('.modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Não</button><button type="button" class="btn btn-danger confirm-delete">Sim</button>');
        $('#modal').modal('toggle');
    });

    $('body').on('click','.confirm-delete', function(e){
        e.preventDefault();
        
        $(".confirm-delete").html('<div class="spinner-border text-primary" role="status"></div>');
        $(".confirm-delete").attr("disabled", true);
        
        $.ajax({
            url : "/api/school/delete/student",
            type : 'post',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : {
                student_id : $('#student_id').val(),
                },
        })
        .done(function(msg){
            $(location).attr('href', '/escola/alunos');
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false); 
        })
        .fail(function(jqXHR, textStatus, msg){
            $(".confirm-delete").html("Sim");
            $(".confirm-delete").attr("disabled", false);  
        }); 
    });


});

	