$( document ).ready(function() {
    
    $("#formUser").submit(function(e){
        e.preventDefault();
        
        $("#btnNew").html('<div class="spinner-border text-primary" role="status"></div>');
        $("#btnNew").attr("disabled", true);
        
        var formData = new FormData(this);
        var url;

        if($('#user_id').val())
        {
            url = "/api/school/update/user";
        }
        else
        {
            url = "/api/school/insert/user";
        }
        
        $.ajax({
            url : url,
            type : 'post',
            processData: false,
            contentType: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            data : formData,
        })
        .done(function(msg){
            
            if(msg == 200)
            {
                $(location).attr('href', '/escola/usuarios');
            }
        })
        .fail(function(jqXHR, textStatus, msg){
            $("#btnNew").html("Incluir");
            $("#btnNew").attr("disabled", false);  
        }); 
    });
});  