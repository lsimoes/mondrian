@extends('parent.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">


		<h5 class="pt-4 pb-4">Calendário</h5>

		<div class="row p-2" style="border-radius: 5px; background-color: #888; color: #FFF;">
			<div class="col-md-2">
				{{$calendar->name}}
			</div>
		</div>
		
		<p class="mt-4" style="font-size: 16px;">
			{{$calendar->description}}
		</p>

		

	</div>
@endsection

@push('scripts')

@endpush