@extends('parent.templates.template1')

@section('content')

	
	<div class="col-lg-8 mx-auto">

		<h5 class="pt-4 pb-4">Eventos</h5>

		@php
			$i = 2;
			$left = '';
			$right = 'float-right';
		@endphp

		@foreach($events as $event)
			@php
				$position = ($i % 2 == 0) ? $left : $right;
			@endphp
			<div class="card w-50 {{$position}}">
			  <div class="card-body">
			    <h5 class="card-title">{{ Carbon\Carbon::parse($event->created_at)->format('d/m/Y') }}</h5>
			    <p class="card-text">{{$event->name}}</p>
			    <a href="eventos/{{$event->id}}" class="btn btn-info">Veja mais</a>
			  </div>
			</div>
			@php
				$i++;
			@endphp
		@endforeach
		
	</div>
@endsection

@push('scripts')
	
@endpush