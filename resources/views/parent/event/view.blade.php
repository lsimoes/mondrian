@extends('parent.templates.template1')

@section('content')

	
	<div class="col-lg-10 mx-auto">

		<h5 class="pt-4 pb-2">{{$event->name}}</h5>
		<p>{{$event->description}}</p>

		<div class="row"> 
			
		</div>

		<div class="row">
		 	@foreach(explode(';', $event->images) as $image) 
			  <div class="col-sm-4 mb-2">
			    <div class="card p-2">
			      
			        <a href="{{ Storage::disk('local')->url('events/') }}{{$image}}" data-toggle="lightbox" data-gallery="gallery">
			  		<img class="card-img-top" src="{{ Storage::disk('local')->url('events/') }}{{$image}}">
				</a>
			      
			    </div>
			  </div>
			@endforeach

			@if($event->videos != '')
				@foreach(explode(PHP_EOL, $event->videos) as $video)
				<div class="col-sm-4 mb-2">
				    <div class="card p-2">
				      
				        <iframe width="100%" src="{{$video}}?controls=1&autoplay=0" frameborder="0" allow="accelerometer; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				      
				    </div>
				  </div>
				@endforeach
			@endif
		</div>
		
		
	</div>
@endsection

@push('css')
	<link rel="stylesheet" type="text/css" href="{{url('js/dist/lightbox/ekko-lightbox.css')}}"/>
@endpush

@push('scripts')
	<script type="text/javascript" src="{{url('js/dist/lightbox/ekko-lightbox.js')}}"></script>
	<script>
		$(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox();
            });
	</script>
@endpush