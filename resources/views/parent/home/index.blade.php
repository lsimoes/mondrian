@extends('parent.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		<div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
          <ol class="carousel-indicators">
            <li data-target="#carouselExampleIndicators" data-slide-to="0" class=""></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="1" class="active"></li>
            <li data-target="#carouselExampleIndicators" data-slide-to="2" class=""></li>
          </ol>
          <div class="carousel-inner" role="listbox">
          	@php ($i = 0)
			@php ($active = '')

          	@foreach($banners as $banner)
          		@if($i == 0)
          			@php ($active = 'active')
          		@else
          			@php ($active = '')
          		@endif
	            <div class="carousel-item {{$active}}">
	              <a href="{{$banner->link}}" target="_blank"><img class="d-block img-fluid" src="{{ Storage::disk('local')->url('banners/') }}{{$banner->image}}" width="1200" height="350" alt="{{$banner->name}}"></a>
	            </div>
	            @php ($i++)
            @endforeach
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>

	    <hr />

	    <h6 class="pt-4 pb-4">Avisos</h6>

	    <table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>Tipo</th>
	                <th>Assunto</th>
	                <th>Turma</th>
	                <th>Data</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($notices as $notice)
	            <tr>
	                <td>
	                	@if($notice->noticeType == 1)
	                		Circular
	                	@else
	                		Comunicado
	                	@endif
	                </td>
	                <td>
	                	<a href="avisos/{{$notice->noticeId}}">
	                		{{$notice->noticeSubject}}
	                	</a>
	                </td>
	                <td>{{$notice->classRoomName}}</td>
	                <td>{{ Carbon\Carbon::parse($notice->noticeDate)->format('d/m/Y') }}</td>
	                <td>
	                	@foreach(explode(';', $notice->noticeAware) as $aware)
	                		@if($aware == Auth::user()->id)
	                			Ciente
	                		@endif
	                	@endforeach
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/parent/home/index.js')}}"></script>
@endpush