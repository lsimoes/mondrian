@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		<a href="{{url('/escola/reunioes/incluir')}}" class="btn btn-success mt-4 float-right">Incluir</a>

		<a href="{{url('/escola/reunioes/calendario')}}" class="btn btn-info mt-4 mr-2 float-right">Ver calendário</a> 

		<h6 class="pt-4 pb-4">Reuniões confirmadas</h6>
		

		<table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>Professora</th>
	                <th>Turma</th>
	                <th>Confirmado por</th>
	                <th>Data</th>
	                <th>Hora</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	            <tr>
	                <td>-</td>
	                <td>-</td>
	                <td>-</td>
	                <td>-</td>
	                <td>-</td>
	                <td>
	                	<button class="btn btn-danger btn-sm"><span class="oi oi-x"></span></button>
	                	<button class="btn btn-primary btn-sm">Editar</button>
	                </td>
	            </tr>
	        </tbody>
	    </table>

	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/calendario/categoria/index.js')}}"></script>
@endpush