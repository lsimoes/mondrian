@extends('parent.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">


		<h5 class="pt-4 pb-4">Reuniões</h5>
		

		<table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	            	<th>Aluno</th>
	                <th>Professora</th>
	                <th>Turma</th>
	                <th>Datas Diponíveis</th>
	                <th>Descrição</th>
	                <th>Status</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($arrMeetings as $meeting)
	        	@php 
	        		//$date = Carbon\Carbon::parse($meeting['meetingDate'])->format('d/m/Y');
	        		$theDate = '';
	        		foreach(explode(';', $meeting['meetingDate']) as $date)
	        		{
	        			$theDate .= Carbon\Carbon::parse($date)->format('d/m/Y') . ' | ';
	        		}

	        		if($meeting['confirm'] != null){

	        			$explode = explode(',', $meeting['confirm']);

	        			$dateConfirm = $explode[1];
	        			$meetingIdConfirm = $explode[4];


	        			if($meetingIdConfirm == $meeting['meetingId'])
	        			{
	        				$hour = $explode[2];
	        				$status = 'Agendado dia ' . Carbon\Carbon::parse($dateConfirm)->format('d/m/Y') . ' as ' . $hour;
	        			}
	        			else
	        			{
	        				$hour = '';
	        				$status = '';
	        			}
	        			
	        			
	        		}
	        		else
	        		{
	        			
	        			$hour = '';
	        			$status = '';
	        		}

	        	@endphp
	            <tr>
	            	<td>{{$meeting['studentName']}}</td>
	                <td>{{$meeting['meetingTeacher']}}</td>
	                <td>{{$meeting['classRoomName']}}</td>
	                <td>{{ $theDate }}</td>
	                <td>{{$meeting['meetingDesc']}}</td>
	                <td>{{$status}}</td>
	                <td>
	                	@if($status == null)
	                		<a href="reunioes/agendar/{{$meeting['meetingId']}}/{{$meeting['studentId']}}" class="btn btn-info btn-sm">Selecionar</a>
	                	@endif
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>

	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/parent/meeting/index.js')}}"></script>
@endpush