@extends('parent.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">


		<h5 class="pt-4 pb-2">Agendar Reunião</h5>
		
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-12 p-2" style="border-radius: 5px; background-color: #999; color: #FFF; font-size: 16px;">
				<div class="col-md-4">
					<strong>Aluno:</strong> {{$meeting->studentName}}
				</div>

				<div class="col-md-4">
					<strong>Professora:</strong> {{$meeting->meetingTeacher}}
				</div>

				<div class="col-md-4">
					<strong>Turma:</strong> {{$meeting->classRoomName}}
				</div>

				<div class="col-md-4">
					<strong>Assunto:</strong> {{$meeting->meetingDescription}}
				</div>
			</div>

			<div class="col-md-12 pt-4">
				<h6 class="pb-4"><strong>Selecione um dia e horário entre as opções disponíveis</strong></h6>

				<form id="formSchedule">
					@foreach(explode(';', $dates) as $date) 
						<div class="row col-md-12 pb-2">
							<strong class="pr-4">{{ Carbon\Carbon::parse($date)->format('d/m/Y') }}:</strong>

							@if(isset($arrHoursSorted[$date]))
								@foreach($arrHoursSorted[$date] as $datehour)
									<div class="form-check form-check-inline">
									  <input class="form-check-input datehour" type="radio" name="datehour" id="{{$date}}{{$datehour['hour']}}" value="{{\Auth::user()->id}},{{$date}},{{$datehour['hour']}},{{$meeting->studentId}},{{$meeting->meetingId}}" required>
									  <label class="form-check-label" for="{{$date}}{{$datehour['hour']}}">{{$datehour['hour']}}</label>
									</div>
								@endforeach
							@endif
						</div>
					@endforeach

					<input type="hidden" id="meeting_id" name="meeting_id" value="{{$meeting->meetingId}}">
					<br>
					<span style="color: red;">Após confirmação, não será possível alterar a data via sistema, caso queira remarcar a reunião, entre em contato com a secretaria.</span>
					<br>
					<button id="btnNew" type="submit" class="btn btn-success mt-4 confirm">Confirmar</button>
				</form>
			</div>

		</div>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/parent/meeting/confirm.js')}}"></script>
@endpush