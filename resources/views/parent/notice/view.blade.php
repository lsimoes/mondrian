@extends('parent.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">


		<h5 class="pt-4 pb-4">Aviso</h5>

		<div class="row p-2" style="border-radius: 5px; background-color: #888; color: #FFF;">
			<div class="col-md-2">
				<strong>TIPO</strong><br>
				@if($notice->noticeType == 1)
            		Circular
            	@else
            		Comunicado
            	@endif
			</div>

			<div class="col-md-4">
				<strong>ASSUNTO</strong><br>
				{{$notice->noticeSubject}}
			</div>

			<div class="col-md-2">
				<strong>TURMA</strong><br>
				{{$notice->classRoomName}}
			</div>

			<div class="col-md-2">
				<strong>CIRCULAR N.</strong><br>
				{{$notice->noticeNumber}}
			</div>

			<div class="col-md-2">
				<strong>DATA</strong><br>
				{{ Carbon\Carbon::parse($notice->noticeDate)->format('d/m/Y') }}
			</div>
		</div>
		
		<p class="mt-4" style="font-size: 16px;">
			{!! $notice->noticeNotice !!}
		</p>

		<div class="col-md-12 text-center"> 
			<input type="hidden" value="{{$notice->noticeId}}" id="notice_id">
				
				@php
					$ciente = 0;

					if($notice->noticeAware == '')
					{
						$ciente = 0;
					}
				@endphp

				@foreach(explode(';', $notice->noticeAware) as $aware)
            		@if($aware == Auth::user()->id)
            			@php $ciente = 1 @endphp
            		@endif
	            @endforeach

	            @if($ciente == 1)
	            	Ciente
	            @else
	            	<button class="btn btn-info mt-4 aware">Estou Ciente</button>
	            @endif
    		
		</div>
		
		

	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/parent/notice/aware.js')}}"></script>
@endpush