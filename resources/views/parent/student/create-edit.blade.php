@extends('parent.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">
		<h5 class="pt-4 pb-4">Matrícula</h5>

		<ul class="nav nav-tabs" id="myTab" role="tablist">
		  <li class="nav-item">
		    <a class="nav-link active" id="aluno-tab" data-toggle="tab" href="#aluno" role="tab" aria-controls="aluno" aria-selected="true">Dados do aluno</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link @if(!isset($student)) disabled @endif" id="medicas-tab" data-toggle="tab" href="#medicas" role="tab" aria-controls="medicas" aria-selected="false">Informações médicas</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link @if(!isset($student)) disabled @endif" id="endereco-tab" data-toggle="tab" href="#endereco" role="tab" aria-controls="endereco" aria-selected="false">Endereço</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link @if(!isset($student)) disabled @endif" id="pais-tab" data-toggle="tab" href="#pais" role="tab" aria-controls="pais" aria-selected="false">Dados dos pais</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link @if(!isset($student)) disabled @endif" id="imagem-tab" data-toggle="tab" href="#imagem" role="tab" aria-controls="imagem" aria-selected="false">Autorização de imagem</a>
		  </li>
		</ul>
		<div class="tab-content" id="myTabContent">
		  	<div class="tab-pane fade show active" id="aluno" role="tabpanel" aria-labelledby="aluno-tab">
			  	<form id="formStudentStep1">
			  		
			  		<div class="col-md-12">
			  			<button type="submit" class="btn btn-success float-right mt-4 btnNew1">
					  		@if(!isset($student))
					  			Próximo >
					  		@else
					  			Salvar
					  		@endif
					  	</button>	
			  		</div>

					<div class="col-md-12 float-left">
						<div class="form-row mt-4">
						    <div class="form-group col-md-6">
						      <label for="name_student">Nome Completo</label>
						      <input type="text" class="form-control" name="name_student" id="name_student" value="@if(isset($student)){{$student->name_student}}@endif" required >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="date_birth_student">Data de Nascimento</label>
						      <input type="date" class="form-control" name="date_birth_student" id="date_birth_student" value="@if(isset($student)){{ $student->date_birth_student }}@endif" required>
						    </div>


						    <div class="form-group col-md-4">
						      <label for="race">Raça</label>
						      <input type="text" class="form-control" name="race" id="race" value="@if(isset($student)){{ $student->race }}@endif" required>
						    </div>

						    <div class="form-group col-md-4">
						      <label for="certificate">Certidão de Nascimento</label>
						      <input type="text" class="form-control" name="certificate" id="certificate" value="@if(isset($student)){{ $student->certificate }}@endif" required>
						    </div>

						    <div class="form-group col-md-4">
						      <label for="rg">RG</label>
						      <input type="text" class="form-control" name="rg" id="rg" value="@if(isset($student)){{ $student->rg }}@endif" required>
						    </div>


						    
						    <div class="form-group col-md-12">
						      <label for="photo">Foto do Aluno</label>
						      <input type="file" class="form-control" name="photo" id="photo" @if(!isset($student)) required @endif>
						      @if(isset($student)) <input type="hidden" value="{{$student->photo}}" class="form-control" name="photo_user" id="photo_user">  @endif
						    </div>
						
						    <div class="form-group col-md-6">
						      <label for="father_power">A criança está sob o pátrio poder do(a):</label>
						      <input type="text" class="form-control" name="father_power" id="father_power" value="@if(isset($student)){{$student->father_power}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="catch_child">Quem, habitualmente, poderá retirar a criança:</label>
						      <input type="text" class="form-control" name="catch_child" id="catch_child" value="@if(isset($student)){{$student->catch_child}}@endif" required>
						    </div>
						</div>
						<div class="form-row">
						    <label for="inputPassword4">Período: &nbsp;</label>
					      	<div class="form-check form-check-inline">
							  <input class="form-check-input" type="radio" name="period" id="period1" value="m" @if(isset($student) && $student->period == 'm') checked @endif required>
							  <label class="form-check-label" for="period1">Manhã</label>
							</div>
							<div class="form-check form-check-inline">
							  <input class="form-check-input" type="radio" name="period" id="period2" value="t" @if(isset($student) && $student->period == 't') checked @endif required>
							  <label class="form-check-label" for="period2">Tarde</label>
							</div>
							<div class="form-check form-check-inline">
							  <input class="form-check-input" type="radio" name="period" id="period3" value="int" @if(isset($student) && $student->period == 'int') checked @endif required>
							  <label class="form-check-label" for="period3">Integral</label>
							</div>
							<div class="form-check form-check-inline">
							  <input class="form-check-input" type="radio" name="period" id="period4" value="ind" @if(isset($student) && $student->period == 'ind') checked @endif required>
							  <label class="form-check-label" for="period4">Intermediário - entrada às 10:00h</label>
							</div>
							<div class="form-check form-check-inline">
							  <input class="form-check-input" type="radio" name="period" id="period5" value="ind2" @if(isset($student) && $student->period == 'ind2') checked @endif required>
							  <label class="form-check-label" for="period5">Intermediário - saída às 14:30h</label>
							</div>
						</div>
						<div class="form-row">
							@php
								if(isset($student) && $student->meals != '')
								{
									$meal = explode(';', $student->meals);
								}
								
								if(isset($student) && $student->brothers != '')
								{
									$brothers = explode(';', $student->brothers);

									if(isset($brothers[0]))
									{
										$brother1 = explode(',', $brothers[0]);
									}
									
									if(isset($brothers[1]))
									{
										$brother2 = explode(',', $brothers[1]);
									}

									if(isset($brothers[2]))
									{
										$brother3 = explode(',', $brothers[2]);
									}

									if(isset($brothers[3]))
									{
										$brother4 = explode(',', $brothers[3]);
									}
									
									
								}

								
							@endphp
						  	<label for="inputPassword4">Refeições: &nbsp;</label>
						  	<div class="form-check form-check-inline">
							  <input class="form-check-input" type="checkbox" name="meals[]" id="meals1" value="a" @if(isset($meal[0]) && $meal[0]  == "a" || isset($meal[1]) && $meal[1]  == "a") checked @endif>
							  <label class="form-check-label" for="meals1">Almoço</label>
							</div>
							<div class="form-check form-check-inline">
							  <input class="form-check-input" type="checkbox" name="meals[]" id="meals2" value="j" @if(isset($meal[0]) && $meal[0]  == "j" || isset($meal[1]) && $meal[1]  == "j") checked @endif>
							  <label class="form-check-label" for="meals2">Jantar</label>
							</div>
							<div class="form-check form-check-inline">
							  <input class="form-check-input" type="checkbox" name="meals[]" id="meals3" value="s" @if(isset($meal[0]) && $meal[0]  == "s" || isset($meal[1]) && $meal[1]  == "s") checked @endif>
							  <label class="form-check-label" for="meals3">Sem Refeição</label>
							</div>
						</div>
					  	<div class="form-row">
					  		<div id="accordion" style="width: 100%">
								<div class="card">
								    <div class="card-header" id="heading1">
								      <h5 class="mb-0">
								        <a class="btn btn-link" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
								          Irmão(ã) 1
								        </a>
								      </h5>
								    </div>

								    <div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#accordion">
								    	<div class="card-body">
								    		<div class="form-row">
									        	<div class="form-group col-md-6">
											      <label for="brothername1">Nome Completo</label>
											      <input type="text" class="form-control" name="brothername1" id="brothername1" placeholder="Nome Completo" value="@if(isset($brother1)) {{$brother1[0]}} @endif">
											    </div>
											    <div class="form-group col-md-6">
											      <label for="brotherage1">Idade</label>
											      <input type="text" class="form-control" name="brotherage1" id="brotherage1" value="@if(isset($brother1)) {{$brother1[1]}} @endif">
											    </div>
											</div>
								      	</div>
								    </div>
								</div>

								<div class="card">
								    <div class="card-header" id="heading2">
								      <h5 class="mb-0">
								        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse2" aria-expanded="true" aria-controls="collapse2">
								          Irmão(ã) 2
								        </a>
								      </h5>
								    </div>

								    <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordion">
								    	<div class="card-body">
								    		<div class="form-row">
									        	<div class="form-group col-md-6">
											      <label for="brothername2">Nome Completo</label>
											      <input type="text" class="form-control" name="brothername2" id="brothername2" placeholder="Nome Completo" value="@if(isset($brother2)) {{$brother2[0]}} @endif">
											    </div>
											    <div class="form-group col-md-6">
											      <label for="brotherage2">Idade</label>
											      <input type="text" class="form-control" name="brotherage2" id="brotherage2" value="@if(isset($brother2)) {{$brother2[1]}} @endif">
											    </div>
											</div>
								      	</div>
								    </div>
								</div>

								<div class="card">
								    <div class="card-header" id="heading3">
								      <h5 class="mb-0">
								        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse3" aria-expanded="true" aria-controls="collapse3">
								          Irmão(ã) 3
								        </a>
								      </h5>
								    </div>

								    <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordion">
								    	<div class="card-body">
								    		<div class="form-row">
									        	<div class="form-group col-md-6">
											      <label for="brothername3">Nome Completo</label>
											      <input type="text" class="form-control" name="brothername3" id="brothername3" placeholder="Nome Completo" value="@if(isset($brother3)) {{$brother3[0]}} @endif">
											    </div>
											    <div class="form-group col-md-6">
											      <label for="brotherage3">Idade</label>
											      <input type="text" class="form-control" name="brotherage3" id="brotherage3" value="@if(isset($brother3)) {{$brother3[1]}} @endif">
											    </div>
											</div>
								      	</div>
								    </div>
								</div>

								<div class="card">
								    <div class="card-header" id="heading4">
								      <h5 class="mb-0">
								        <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse4" aria-expanded="true" aria-controls="collapse4">
								          Irmão(ã) 4
								        </a>
								      </h5>
								    </div>

								    <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordion">
								    	<div class="card-body">
								    		<div class="form-row">
									        	<div class="form-group col-md-6">
											      <label for="brothername4">Nome Completo</label>
											      <input type="text" class="form-control" name="brothername4" id="brothername4" placeholder="Nome Completo" value="@if(isset($brother4)) {{$brother4[0]}} @endif">
											    </div>
											    <div class="form-group col-md-6">
											      <label for="brotherage4">Idade</label>
											      <input type="text" class="form-control" name="brotherage4" id="brotherage4" value="@if(isset($brother4)) {{$brother4[1]}} @endif">
											    </div>
											</div>
								      	</div>
								    </div>
								</div>

							</div>
					  	</div>
					  	
						<input type="hidden" value="@if(isset($student)) {{$student->id}} @endif" class="student_id" name="student_id">

						<input type="hidden" value="{{Request::segment(3)}}" class="place_url" name="place_url">
					</div>
					
				  	
				</form>
		  	</div>
		  	<div class="tab-pane fade" id="medicas" role="tabpanel" aria-labelledby="medicas-tab">
		 		<form id="formStudentStep2">

		 			<div class="col-md-12">
			  			<button type="submit" class="btn btn-success float-right mt-4 mb-4 btnNew2">
					  		@if(!isset($student))
					  			Próximo >
					  		@else
					  			Salvar
					  		@endif
					  	</button>	
			  		</div>

			  		<div class="col-md-12 float-left">
						<div class="form-row mt-4">
						    <div class="form-group col-md-6">
						      <label for="allergy">Possui alguma alergia:</label>
						      <input type="text" class="form-control" name="allergy" id="allergy" value="@if(isset($student)){{$student->allergy}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="food_restriction">Possui alguma Restrição Alimentar:</label>
						      <input type="text" class="form-control" name="food_restriction" id="food_restriction" value="@if(isset($student)){{$student->food_restriction}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="medicines_often">Toma algum medicamentos frequentemente:</label>
						      <input type="text" class="form-control" name="medicines_often" id="medicines_often" value="@if(isset($student)){{$student->medicines_often}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						    	<label for="blood_group">Grupo Sanguíneo:</label>
						      	<select class="form-control" name="blood_group" id="blood_group">
						      		<option value="">Selecione</option>
						      		<option @if(isset($student) && $student->blood_group == 'O+') selected @endif>O+</option>
						      		<option @if(isset($student) && $student->blood_group == 'A+') selected @endif>A+</option>
						      		<option @if(isset($student) && $student->blood_group == 'B+') selected @endif>B+</option>
						      		<option @if(isset($student) && $student->blood_group == 'AB+') selected @endif>AB+</option>
						      		<option @if(isset($student) && $student->blood_group == 'O-') selected @endif>O-</option>
						      		<option @if(isset($student) && $student->blood_group == 'A-') selected @endif>A-</option>
						      		<option @if(isset($student) && $student->blood_group == 'B-') selected @endif>B-</option>
						      		<option @if(isset($student) && $student->blood_group == 'AB-') selected @endif>AB-</option>
						      	</select>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="medical_agreement">Possui Convênio Médico: Qual:</label>
						      <input type="text" class="form-control" name="medical_agreement" id="medical_agreement" value="@if(isset($student)){{$student->medical_agreement}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="fever_case">Em caso de febre:</label>
						      <input type="text" class="form-control" name="fever_case" id="fever_case" value="@if(isset($student)){{$student->fever_case}}@endif" required>
						    </div>
						    <div class="form-group col-md-12">
						      <label for="health_observations">Outras observações de saúde:</label>
						      <textarea class="form-control" name="health_observations" id="health_observations" value="@if(isset($student)){{$student->health_observations}}@endif" ></textarea>
						    </div>
						</div>
						
						<input type="hidden" value="@if(isset($student)) {{$student->id}} @endif" class="student_id" name="student_id">

						<input type="hidden" value="{{Request::segment(3)}}" name="place_url">

						
					</div>
				</form>	
		  	</div>
		  	<div class="tab-pane fade" id="endereco" role="tabpanel" aria-labelledby="endereco-tab">
		  		<form id="formStudentStep3">

		  			<div class="col-md-12">
			  			<button type="submit" class="btn btn-success float-right mt-4 mb-4 btnNew3">
					  		@if(!isset($student))
					  			Próximo >
					  		@else
					  			Salvar
					  		@endif
					  	</button>	
			  		</div>

			  		<div class="col-md-12 float-left">
				  		<div class="form-row mt-4">
							<div class="form-group col-md-12">
								<h5>Endereço do Aluno</h5>
							</div>
						    <div class="form-group col-md-6">
						      <label for="cep_student">CEP</label>
						      <input type="text" class="form-control" name="cep_student" id="cep_student" placeholder="Cep" value="@if(isset($student)){{$student->cep_student}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="address_student">Endereço</label>
						      <input type="text" class="form-control" name="address_student" id="address_student" value="@if(isset($student)){{$student->address_student}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="number_student">Número</label>
						      <input type="text" class="form-control" name="number_student" id="number_student" value="@if(isset($student)){{$student->number_student}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="complement_student">Complemento</label>
						      <input type="text" class="form-control" name="complement_student" id="complement_student" value="@if(isset($student)){{$student->complement_student}}@endif">
						    </div>
						    <div class="form-group col-md-6">
						      <label for="neighborhood_student">Bairro</label>
						      <input type="text" class="form-control" name="neighborhood_student" id="neighborhood_student" value="@if(isset($student)){{$student->neighborhood_student}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="city_student">Cidade</label>
						      <input type="text" class="form-control" name="city_student" id="city_student" value="@if(isset($student)){{$student->city_student}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      	<label for="state_student">Estado</label>
						      	<input type="text" class="form-control" name="state_student" id="state_student" value="@if(isset($student)){{$student->state_student}}@endif" required readonly>

						      	<!--<select class="form-control" name="state_student" id="state_student" required>
									<option value="">Selecione seu Estado</option>
									<option value="Acre">Acre</option>
									<option value="Alagoas">Alagoas</option>
									<option value="Amapá">Amapá</option>
									<option value="Amazonas">Amazonas</option>
									<option value="Bahia">Bahia</option>
									<option value="Ceará">Ceará</option>
									<option value="Distrito Federal">Distrito Federal</option>
									<option value="Espírito Santo">Espírito Santo</option>
									<option value="Goiás">Goiás</option>
									<option value="Pará">Pará</option>
									<option value="Paraíba">Paraíba</option>
									<option value="Paraná">Paraná</option>
									<option value="Pernambuco">Pernambuco</option>
									<option value="Piauí">Piauí</option>
									<option value="Maranhão">Maranhão</option>
									<option value="Mato Grosso">Mato Grosso</option>
									<option value="Mato Grosso do Sul">Mato Grosso do Sul</option>
									<option value="Minas Gerais">Minas Gerais</option>
									<option value="Rio de Janeiro">Rio de Janeiro</option>
									<option value="Rio Grande do Norte">Rio Grande do Norte</option>
									<option value="Rio Grande do Sul">Rio Grande do Sul</option>
									<option value="Rondônia">Rondônia</option>
									<option value="Roraima">Roraima</option>
									<option value="Santa Catarina">Santa Catarina</option>
									<option value="São Paulo">São Paulo</option>
									<option value="Sergipe">Sergipe</option>
									<option value="Tocantins">Tocantins</option>
								</select>-->
						    </div>
						</div>
						
						<input type="hidden" value="@if(isset($student)) {{$student->id}} @endif" class="student_id" name="student_id">

						<input type="hidden" value="{{Request::segment(3)}}" name="place_url">

						
					</div>
				</form>
		  	</div>
		  	<div class="tab-pane fade" id="pais" role="tabpanel" aria-labelledby="pais-tab">
		  		<form id="formStudentStep4">

		  			<div class="col-md-12">
			  			<button type="submit" class="btn btn-success float-right mt-4 mb-4 btnNew4">
					  		@if(!isset($student))
					  			Próximo >
					  		@else
					  			Salvar
					  		@endif
					  	</button>
			  		</div>

		  			<div class="col-md-12 float-left">
						<div class="form-row mt-4">
							<div class="form-group col-md-12">
								<h5>Filiação 1</h5>
							</div>
						    <div class="form-group col-md-6">
						      <label for="name_dad">Nome</label>
						      <input type="text" class="form-control" name="name_dad" id="name_dad" value="@if(isset($student)){{$student->name_dad}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="date_birth_dad">Data de Nascimento</label>
						      <input type="date" class="form-control" name="date_birth_dad" id="date_birth_dad" value="@if(isset($student)){{$student->date_birth_dad}}@endif">
						    </div>
						    <div class="form-group col-md-6">
						      <label for="cpf_dad">CPF</label>
						      <input type="text" class="form-control" name="cpf_dad" id="cpf_dad" value="@if(isset($student)){{$student->cpf_dad}}@endif">
						    </div>
						    <div class="form-group col-md-6">
						      <label for="email_dad">E-mail</label>
						      <input type="email" class="form-control" name="email_dad" id="email_dad" value="@if(isset($student)){{$student->email_dad}}@endif">
						    </div>
						    <div class="form-group col-md-6">
						      <label for="cell_dad">Celular</label>
						      <input type="text" class="form-control" name="cell_dad" id="cell_dad" value="@if(isset($student)){{$student->cell_dad}}@endif">
						    </div>
						    <div class="form-group col-md-6">
						      <label for="job_dad">Profissão</label>
						      <input type="text" class="form-control" name="job_dad" id="job_dad" value="@if(isset($student)){{$student->job_dad}}@endif">
						    </div>
						</div>

						<div class="form-row mt-4">
							<div class="form-group col-md-12">
								<h5>Endereço Comercial</h5>
							</div>
						    <div class="form-group col-md-6">
						      <label for="comm_tel_dad">Telefone Comercial</label>
						      <input type="text" class="form-control" name="comm_tel_dad" id="comm_tel_dad" value="@if(isset($student)){{$student->comm_tel_dad}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_cep_dad">CEP</label>
						      <input type="text" class="form-control" name="comm_cep_dad" id="comm_cep_dad" value="@if(isset($student)){{$student->comm_cep_dad}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_address_dad">Endereço</label>
						      <input type="text" class="form-control" name="comm_address_dad" id="comm_address_dad" value="@if(isset($student)){{$student->comm_address_dad}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_number_dad">Número</label>
						      <input type="text" class="form-control" name="comm_number_dad" id="comm_number_dad" value="@if(isset($student)){{$student->comm_number_dad}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_complement_dad">Complemento</label>
						      <input type="text" class="form-control" name="comm_complement_dad" id="comm_complement_dad" value="@if(isset($student)){{$student->comm_complement_dad}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_neighborhood_dad">Bairro</label>
						      <input type="text" class="form-control" name="comm_neighborhood_dad" id="comm_neighborhood_dad" value="@if(isset($student)){{$student->comm_neighborhood_dad}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_city_dad">Cidade</label>
						      <input type="text" class="form-control" name="comm_city_dad" id="comm_city_dad" value="@if(isset($student)){{$student->comm_city_dad}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_state_dad">Estado</label>
						      <input type="text" class="form-control" name="comm_state_dad" id="comm_state_dad" value="@if(isset($student)){{$student->comm_state_dad}}@endif" readonly>

						      	<!--<select class="form-control" name="comm_state_dad" id="comm_state_dad">
									<option value="">Selecione...</option>
									<option value="AC" @if(isset($student) && $student->comm_state_dad == 'AC') selected @endif>Acre</option>
									<option value="AL" @if(isset($student) && $student->comm_state_dad == 'AL') selected @endif>Alagoas</option>
									<option value="AP" @if(isset($student) && $student->comm_state_dad == 'AP') selected @endif>Amapá</option>
									<option value="AM" @if(isset($student) && $student->comm_state_dad == 'AM') selected @endif>Amazonas</option>
									<option value="BA" @if(isset($student) && $student->comm_state_dad == 'BA') selected @endif>Bahia</option>
									<option value="CE" @if(isset($student) && $student->comm_state_dad == 'CE') selected @endif>Ceará</option>
									<option value="DF" @if(isset($student) && $student->comm_state_dad == 'DF') selected @endif>Distrito Federal</option>
									<option value="ES" @if(isset($student) && $student->comm_state_dad == 'ES') selected @endif>Espírito Santo</option>
									<option value="GO" @if(isset($student) && $student->comm_state_dad == 'GO') selected @endif>Goiás</option>
									<option value="PA" @if(isset($student) && $student->comm_state_dad == 'PA') selected @endif>Pará</option>
									<option value="PB" @if(isset($student) && $student->comm_state_dad == 'PB') selected @endif>Paraíba</option>
									<option value="PR" @if(isset($student) && $student->comm_state_dad == 'PR') selected @endif>Paraná</option>
									<option value="PE" @if(isset($student) && $student->comm_state_dad == 'PE') selected @endif>Pernambuco</option>
									<option value="PI" @if(isset($student) && $student->comm_state_dad == 'PI') selected @endif>Piauí</option>
									<option value="MA" @if(isset($student) && $student->comm_state_dad == 'MA') selected @endif>Maranhão</option>
									<option value="MT" @if(isset($student) && $student->comm_state_dad == 'MT') selected @endif>Mato Grosso</option>
									<option value="MS" @if(isset($student) && $student->comm_state_dad == 'MS') selected @endif>Mato Grosso do Sul</option>
									<option value="MG" @if(isset($student) && $student->comm_state_dad == 'MG') selected @endif>Minas Gerais</option>
									<option value="RJ" @if(isset($student) && $student->comm_state_dad == 'RJ') selected @endif>Rio de Janeiro</option>
									<option value="RN" @if(isset($student) && $student->comm_state_dad == 'RN') selected @endif>Rio Grande do Norte</option>
									<option value="RS" @if(isset($student) && $student->comm_state_dad == 'RS') selected @endif>Rio Grande do Sul</option>
									<option value="RO" @if(isset($student) && $student->comm_state_dad == 'RO') selected @endif>Rondônia</option>
									<option value="RR" @if(isset($student) && $student->comm_state_dad == 'RR') selected @endif>Roraima</option>
									<option value="SC" @if(isset($student) && $student->comm_state_dad == 'SC') selected @endif>Santa Catarina</option>
									<option value="SP" @if(isset($student) && $student->comm_state_dad == 'SP') selected @endif>São Paulo</option>
									<option value="SE" @if(isset($student) && $student->comm_state_dad == 'SE') selected @endif>Sergipe</option>
									<option value="TO" @if(isset($student) && $student->comm_state_dad == 'TO') selected @endif>Tocantins</option>
								</select>-->
						    </div>
						</div>

						<hr />

						<div class="form-row mt-4">
							<div class="form-group col-md-12">
								<h5>Filiação 2</h5>
							</div>
						    <div class="form-group col-md-6">
						      <label for="name_mom">Nome</label>
						      <input type="text" class="form-control" name="name_mom" id="name_mom" value="@if(isset($student)){{$student->name_mom}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="date_birth_mom">Data de Nascimento</label>
						      <input type="date" class="form-control" name="date_birth_mom" id="date_birth_mom" value="@if(isset($student)){{$student->date_birth_mom}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="cpf_mom">CPF</label>
						      <input type="text" class="form-control" name="cpf_mom" id="cpf_mom" value="@if(isset($student)){{$student->cpf_mom}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="email_mom">E-mail</label>
						      <input type="email" class="form-control" name="email_mom" id="email_mom" value="@if(isset($student)){{$student->email_mom}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="cell_mom">Celular</label>
						      <input type="text" class="form-control" name="cell_mom" id="cell_mom" value="@if(isset($student)){{$student->cell_mom}}@endif" required>
						    </div>
						    <div class="form-group col-md-6">
						      <label for="job_mom">Profissão</label>
						      <input type="text" class="form-control" name="job_mom" id="job_mom" value="@if(isset($student)){{$student->job_mom}}@endif" required>
						    </div>
						</div>

						<div class="form-row mt-4">
							<div class="form-group col-md-12">
								<h5>Endereço Comercial</h5>
							</div>
						    <div class="form-group col-md-6">
						      <label for="comm_tel_mom">Telefone Comercial</label>
						      <input type="text" class="form-control" name="comm_tel_mom" id="comm_tel_mom" value="@if(isset($student)){{$student->comm_tel_mom}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_cep_mom">CEP</label>
						      <input type="text" class="form-control" name="comm_cep_mom" id="comm_cep_mom" value="@if(isset($student)){{$student->comm_cep_mom}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_address_mom">Endereço</label>
						      <input type="text" class="form-control" name="comm_address_mom" id="comm_address_mom" value="@if(isset($student)){{$student->comm_address_mom}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_number_mom">Número</label>
						      <input type="text" class="form-control" name="comm_number_mom" id="comm_number_mom" value="@if(isset($student)){{$student->comm_number_mom}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_complement_mom">Complemento</label>
						      <input type="text" class="form-control" name="comm_complement_mom" id="comm_complement_mom" value="@if(isset($student)){{$student->comm_complement_mom}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_neighborhood_mom">Bairro</label>
						      <input type="text" class="form-control" name="comm_neighborhood_mom" id="comm_neighborhood_mom" value="@if(isset($student)){{$student->comm_neighborhood_mom}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_city_mom">Cidade</label>
						      <input type="text" class="form-control" name="comm_city_mom" id="comm_city_mom" value="@if(isset($student)){{$student->comm_city_mom}}@endif" >
						    </div>
						    <div class="form-group col-md-6">
						      <label for="comm_state_mom">Estado</label>
						      <input type="text" class="form-control" name="comm_state_mom" id="comm_state_mom" value="@if(isset($student)){{$student->comm_state_mom}}@endif" readonly>

						      	<!--<select class="form-control" name="comm_state_mom" id="comm_state_mom" >
									<option value="">Selecione...</option>
									<option value="AC" @if(isset($student) && $student->comm_state_mom == 'AC') selected @endif>Acre</option>
									<option value="AL" @if(isset($student) && $student->comm_state_mom == 'AL') selected @endif>Alagoas</option>
									<option value="AP" @if(isset($student) && $student->comm_state_mom == 'AP') selected @endif>Amapá</option>
									<option value="AM" @if(isset($student) && $student->comm_state_mom == 'AM') selected @endif>Amazonas</option>
									<option value="BA" @if(isset($student) && $student->comm_state_mom == 'BA') selected @endif>Bahia</option>
									<option value="CE" @if(isset($student) && $student->comm_state_mom == 'CE') selected @endif>Ceará</option>
									<option value="DF" @if(isset($student) && $student->comm_state_mom == 'DF') selected @endif>Distrito Federal</option>
									<option value="ES" @if(isset($student) && $student->comm_state_mom == 'ES') selected @endif>Espírito Santo</option>
									<option value="GO" @if(isset($student) && $student->comm_state_mom == 'GO') selected @endif>Goiás</option>
									<option value="PA" @if(isset($student) && $student->comm_state_mom == 'PA') selected @endif>Pará</option>
									<option value="PB" @if(isset($student) && $student->comm_state_mom == 'PB') selected @endif>Paraíba</option>
									<option value="PR" @if(isset($student) && $student->comm_state_mom == 'PR') selected @endif>Paraná</option>
									<option value="PE" @if(isset($student) && $student->comm_state_mom == 'PE') selected @endif>Pernambuco</option>
									<option value="PI" @if(isset($student) && $student->comm_state_mom == 'PI') selected @endif>Piauí</option>
									<option value="MA" @if(isset($student) && $student->comm_state_mom == 'MA') selected @endif>Maranhão</option>
									<option value="MT" @if(isset($student) && $student->comm_state_mom == 'MT') selected @endif>Mato Grosso</option>
									<option value="MS" @if(isset($student) && $student->comm_state_mom == 'MS') selected @endif>Mato Grosso do Sul</option>
									<option value="MG" @if(isset($student) && $student->comm_state_mom == 'MG') selected @endif>Minas Gerais</option>
									<option value="RJ" @if(isset($student) && $student->comm_state_mom == 'RJ') selected @endif>Rio de Janeiro</option>
									<option value="RN" @if(isset($student) && $student->comm_state_mom == 'RN') selected @endif>Rio Grande do Norte</option>
									<option value="RS" @if(isset($student) && $student->comm_state_mom == 'RS') selected @endif>Rio Grande do Sul</option>
									<option value="RO" @if(isset($student) && $student->comm_state_mom == 'RO') selected @endif>Rondônia</option>
									<option value="RR" @if(isset($student) && $student->comm_state_mom == 'RR') selected @endif>Roraima</option>
									<option value="SC" @if(isset($student) && $student->comm_state_mom == 'SC') selected @endif>Santa Catarina</option>
									<option value="SP" @if(isset($student) && $student->comm_state_mom == 'SP') selected @endif>São Paulo</option>
									<option value="SE" @if(isset($student) && $student->comm_state_mom == 'SE') selected @endif>Sergipe</option>
									<option value="TO" @if(isset($student) && $student->comm_state_mom == 'TO') selected @endif>Tocantins</option>
								</select>-->
						    </div>
						</div>
						
						<input type="hidden" value="@if(isset($student)) {{$student->id}} @endif" class="student_id" name="student_id">

						<input type="hidden" value="{{Request::segment(3)}}" name="place_url">

						
					</div>
				</form>
		  	</div>
		  	<div class="tab-pane fade" id="imagem" role="tabpanel" aria-labelledby="imagem-tab">
		  		<form id="formStudentStep5">

		  			<div class="col-md-12">
			  			<button type="submit" class="btn btn-success float-right mt-4 mb-4 btnNew5">
					  		@if(!isset($student))
					  			Finalizar
					  		@else
					  			Salvar
					  		@endif
					  	</button>
			  		</div>

			  		<div class="col-md-12 float-left">
				  		<h4 class="mt-4">Autorização de Uso de Imagem</h4>
				  		<p>
				  			A Recreação Infantil Mondrian, livre de quaisquer ônus para com o aluno(a) e seu(sua) responsável, solicita a autorização para utilizar-se da sua imagem para fins exclusivos de divulgação da escola e suas atividades não publicitárias. Podendo, para tanto, reproduzi-la ou divulga-la junto a Internet, no site, redes sociais abertas, revistas, jornais e todos os demais meios de comunicação, público ou privado. 
				  		</p>

				  		<div class="form-check form-check-inline col-md-12">
						  <input class="form-check-input" type="radio" name="image_authorization" id="image_authorization1" value="1" @if(isset($student) && $student->image_authorization == '1') checked @endif required>
						  <label class="form-check-label" for="image_authorization1">Autorizo a utilização da imagem durante o ano letivo, conforme previsto nos termos acima.</label>
						</div>

						<div class="form-check form-check-inline col-md-12">
						  <input class="form-check-input" type="radio" name="image_authorization" id="image_authorization2" value="2" @if(isset($student) && $student->image_authorization == '2') checked @endif required>
						  <label class="form-check-label" for="image_authorization2">Autorizo sua participação apenas nas fotos da rotina, expostas dentro da escola, redes sociais restritas e do site da escola e nas imagens coladas nas apostilas dos alunos.</label>
						</div>

						<div class="form-check form-check-inline col-md-12">
						  <input class="form-check-input" type="radio" name="image_authorization" id="image_authorization3" value="3" @if(isset($student) && $student->image_authorization == '3') checked @endif required>
						  <label class="form-check-label" for="image_authorization3">Não autorizo a utilização de imagem de maneira nenhuma.</label>
						</div>
						
						<input type="hidden" value="@if(isset($student)) {{$student->id}} @endif" class="student_id" name="student_id">

						<input type="hidden" value="{{Request::segment(3)}}" name="place_url">
					</div>
				  	
				</form>
		  	</div>
		  	
		</div>

		
	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/parent/student/create-edit.js')}}"></script>
@endpush