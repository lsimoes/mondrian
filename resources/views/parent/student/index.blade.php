@extends('parent.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		<button class="btn btn-success mt-4 float-right newstudent">Nova matrícula </button> 

		<h5 class="pt-4 pb-4">Alunos</h5>
		

		<table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	            	<th>Foto</th>
	                <th>Nome</th>
	                <th>Turma</th>
	                <th>Status</th>
	                <th>RI</th>
	                <th>Última alteração</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($students as $student)
	            <tr>
	            	<td><img src="{{ Storage::disk('local')->url('students/') }}{{$student->studentPhoto}}" width="100" /></td>
	                <td>{{$student->studentName}}</td>
	                <td>{{$student->classRoomName}}</td>
	                <td>
	                	@if($student->studentStatus == 2)
	                		Aguardando aprovação
	                	@elseif ($student->studentStatus == 0)
	                		Aguardando preenchimento
	                	@elseif ($student->studentStatus == 1)
	                		Matriculado {{$student->studentMatriculationDate}}
	                	@endif
	                </td>
	                <td>{{$student->studentRiNumber}}</td>
	                <td>{{ Carbon\Carbon::parse($student->studentUpdatedAt)->format('d/m/Y H:m') }}</td>
	                <td>
	                	<button class="btn btn-danger btn-sm delete" data-id="{{$student->studentId}}"><span class="oi oi-x"></span></button>
	                	<a href="alunos/editar/{{$student->studentId}}" class="btn btn-primary btn-sm">Editar</a>
	                	@if($student->studentStatus == 1)
	                		<button class="btn btn-success btn-sm rematch" data-id="{{$student->studentId}}">Rematricular</button>
	                	@endif
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>

	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/parent/student/index.js')}}"></script>
@endpush