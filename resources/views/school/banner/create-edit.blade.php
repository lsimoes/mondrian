@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		

		<h6 class="pt-4 pb-4">Banner interno</h6>

		<form id="formBanner">
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="name">Nome</label>
		      <input type="text" class="form-control" name="name" id="name" placeholder="Nome" @if(isset($banner)) value="{{$banner->name}}" @endif required>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="order">Ordem</label>
		      <input type="number" class="form-control" name="order" id="order" placeholder="Ordem" @if(isset($banner)) value="{{$banner->order}}" @endif required>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="image">Imagem</label>
		    <input type="file" class="form-control" name="image" id="image" required>
		  </div>
		  <div class="form-group">
		    <label for="link">Link</label>
		    <input type="text" class="form-control" name="link" id="link" placeholder="Link" @if(isset($banner)) value="{{$banner->link}}" @endif required>
		  </div>
		  <input type="hidden" id="banner_id" name="banner_id" @if(isset($banner)) value="{{$banner->id}}" @endif>
		  <button id="btnNew" type="submit" class="btn btn-success float-right">Salvar</button>
		</form>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/banner/create-edit.js')}}"></script>
@endpush