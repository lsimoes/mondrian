@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">


		<a href="{{url('/escola/banners/incluir')}}" class="btn btn-success mt-4 float-right">Incluir</a>
		<h5 class="pt-4 pb-4">Banner interno</h5>
		

		<table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>Thumb</th>
	                <th>Nome</th>
	                <th>Ordem</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($banners as $banner)
	            <tr>
	                <td><img src="{{ Storage::disk('local')->url('banners/') }}{{$banner->image}}" width="300" /> </td>
	                <td>{{$banner->name}}</td>
	                <td>{{$banner->order}}</td>
	                <td>
	                	<button class="btn btn-danger btn-sm delete" data-id="{{$banner->id}}"><span class="oi oi-x"></span></button>
	                	<a href="banners/editar/{{$banner->id}}" class="btn btn-primary btn-sm">Editar</a>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>

	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/banner/index.js')}}"></script>
@endpush