@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">
		<h6 class="pt-4 pb-4">Nova categoria do calendário</h6>

		<form id="formCalendarCategory">
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="name">Nome</label>
		      <input type="text" class="form-control" name="name" id="name" placeholder="Nome" @if(isset($calendar_category)) value="{{$calendar_category->name}}" @endif required>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="color">Cor</label>
		      <input type="color" class="form-control" name="color" id="color" placeholder="Color" @if(isset($calendar_category)) value="{{$calendar_category->color}}" @endif required>
		    </div>
		  </div>
		  <input type="hidden" id="calendar_category_id" name="calendar_category_id" @if(isset($calendar_category)) value="{{$calendar_category->id}}" @endif>
		  <button id="btnNew" type="submit" class="btn btn-success float-right">Salvar</button>
		</form>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/calendar/category/create-edit.js')}}"></script>
@endpush