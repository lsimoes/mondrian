@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		<a href="{{url('/escola/calendarios/categorias/adicionar')}}" class="btn btn-success mt-4 float-right">Incluir</a>
		<h6 class="pt-4 pb-4">Categorias</h6>
		

		<table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>Cor</th>
	                <th>Nome</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($calendar_categories as $calendar_category)
	            <tr>
	                <td bgcolor="{{$calendar_category->color}}"></td>
	                <td>{{$calendar_category->name}}</td>
	                <td>
	                	<button class="btn btn-danger btn-sm delete" data-id="{{$calendar_category->id}}"><span class="oi oi-x"></span></button>
	                	<a href="categorias/editar/{{$calendar_category->id}}" class="btn btn-primary btn-sm">Editar</a>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>

	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/calendar/category/index.js')}}"></script>
@endpush