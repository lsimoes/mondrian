@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">
		<h6 class="pt-4 pb-4">Novo evento no calendário</h6>

		<form id="formCalendar">
		  <div class="form-row">
		    <div class="form-group col-md-12">
		      <label for="name">Nome</label>
		      <input type="text" class="form-control" name="name" id="name" placeholder="Nome" @if(isset($calendar)) value="{{$calendar->name}}" @endif required>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="class_room_id">Turma</label>
		      <select class="form-control" id="class_room_id" name="class_room_id" required>
		      	<option value="">Selecione</option>
		      	@foreach($class_rooms as $class_room)
		      		<option value="{{$class_room->id}}" @if(isset($calendar) && $calendar->class_room_id == $class_room->id) selected @endif>{{$class_room->name}}</option>
		      	@endforeach
		      </select>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="calendar_category_id">Categoria</label>
		      <select class="form-control" id="calendar_category_id" name="calendar_category_id" required>
		      	<option value="">Selecione</option>
		      	@foreach($calendar_categories as $calendar_category)
		      		<option value="{{$calendar_category->id}}" @if(isset($calendar) && $calendar->calendar_category_id == $calendar_category->id) selected @endif>{{$calendar_category->name}}</option>
		      	@endforeach
		      </select>
		      <!-- <a href="{{url('calendario/categoria/adicionar')}}">Adicionar</a> -->
		    </div>
		    <div class="form-group col-md-6">
		      <label for="date">Data</label>
		      <input type="date" class="form-control" name="date" id="date" placeholder="Data" @if(isset($calendar)) value="{{$calendar->date}}" @endif required>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="period">Período</label><br>
		      	<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="period" id="period1" value="m" @if(isset($calendar) && $calendar->period == 'm') checked @endif  required>
				  <label class="form-check-label" for="period1">Manhã</label>
				</div>
				<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="period" id="period2" value="t" @if(isset($calendar) && $calendar->period == 't') checked @endif required>
				  <label class="form-check-label" for="period2">Tarde</label>
				</div>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="description">Descrição</label>
		    <textarea class="form-control" name="description" id="description" rows="5" required>@if(isset($calendar)) {{$calendar->description}} @endif</textarea>
		  </div>
		  <input type="hidden" id="calendar_id" name="calendar_id" @if(isset($calendar)) value="{{$calendar->id}}" @endif>
		  @if(isset($calendar))
		  	<button class="btn btn-danger float-left delete" data-id='{{$calendar->id}}'>Excluir</button>
		  @endif
		  <button id="btnNew" type="submit" class="btn btn-success float-right">Salvar</button>
		</form>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/calendar/create-edit.js')}}"></script>
@endpush