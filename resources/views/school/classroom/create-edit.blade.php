@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">
		<h6 class="pt-4 pb-4">Nova Turma</h6>

		<form id="formClass">
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="teacher">Teacher</label>
		      <input type="text" class="form-control" id="teacher" name="teacher" placeholder="Professor"  @if(isset($class_room)) value="{{$class_room->teacher}}" @endif required>
		    </div>
		  </div>
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="name">Nome</label>
		      <input type="text" class="form-control" id="name" name="name" placeholder="Nome"  @if(isset($class_room)) value="{{$class_room->name}}" @endif required>
		    </div>
		  </div>
		  <input type="hidden" id="class_room_id" name="class_room_id" @if(isset($class_room)) value="{{$class_room->id}}" @endif>
		  <button id="btnNew" type="submit" class="btn btn-success">Salvar</button>
		</form>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/classroom/create-edit.js')}}"></script>
@endpush