@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		<a href="{{url('/escola/turmas/incluir')}}" class="btn btn-success mt-4 float-right">Incluir</a> 

		<h5 class="pt-4 pb-4">Turmas</h5>
		

		<table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	            	<th>Professor</th>
	                <th>Turma</th>
	                <th>Matriculados</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($arrClassrooms as $classroom)
	            <tr>
	            	<td>{{$classroom['teacher']}}</td>
	                <td>{{$classroom['name']}}</td>
	                <td>{{$classroom['quantity']}}</td>
	                <td>
	                	<button class="btn btn-danger btn-sm delete" data-id="{{$classroom['id']}}"><span class="oi oi-x"></span></button>
	                	<a href="/escola/turmas/editar/{{$classroom['id']}}" class="btn btn-primary btn-sm">Editar</button>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>

	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/classroom/index.js')}}"></script>
@endpush