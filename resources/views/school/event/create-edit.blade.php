@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">
		<h6 class="pt-4 pb-4">Novo evento</h6>

		<form id="formEvent">
		  <div class="form-row">
		    <div class="form-group col-md-12">
		      <label for="name">Nome</label>
		      <input type="text" class="form-control" name="name" id="name" placeholder="Nome" @if(isset($event)) value="{{$event->name}}" @endif required>
		    </div>

		    <div class="form-group col-md-6">
		      <label for="date">Data</label>
		      <input type="date" class="form-control" name="date" id="date" placeholder="Data" @if(isset($event)) value="{{$event->date}}" @endif required>
		    </div>
		    <div class="form-group col-md-6">
		      <label>Período</label><br>
		      	<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="period" id="period1" value="m" @if(isset($event) && $event->period == 'm') checked @endif required>
				  <label class="form-check-label" for="period1">Manhã</label>
				</div>
				<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="period" id="period2" value="t" @if(isset($event) && $event->period == 't') checked @endif required>
				  <label class="form-check-label" for="period2">Tarde</label>
				</div>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="description">Descrição</label>
		    <textarea class="form-control" rows="5" name="description" id="description" required>@if(isset($event)) {{$event->description}} @endif</textarea>
		  </div>
		  <div class="form-group">
		      <label for="images">Imagens</label>
		      <input type="file" class="form-control" name="images[]" id="images" placeholder="Imagens" multiple @if(!isset($event)) required @endif>
		    </div>
		  <div class="form-group">
		    <label for="videos">Videos</label>
		    <textarea name="videos" id="videos" class="form-control" rows="5">@if(isset($event)) {{$event->videos}} @endif</textarea>
		  </div>
		  <input type="hidden" id="event_id" name="event_id" @if(isset($event)) value="{{$event->id}}" @endif>
		  <button type="submit" id="btnNew" class="btn btn-success float-right mb-4">Salvar</button>
		</form>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/event/create-edit.js')}}"></script>
@endpush