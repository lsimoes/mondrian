@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		<a href="{{url('/escola/eventos/incluir')}}" class="btn btn-success mt-4 float-right">Incluir</a> 

		<h5 class="pt-4 pb-4">Eventos</h5>
		

		<table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>Nome</th>
	                <th>Data</th>
	                <th>Status</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($events as $event)
	            <tr>
	                <td>{{$event->name}}</td>
	                <td>{{ Carbon\Carbon::parse($event->date)->format('d/m/Y') }}</td>
	                <td>
	                	@if ($event->status == 0) 
	                		Aguardando Aprovação
	                	@else
	                		Publicado
	                	@endif
	                </td>
	                <td>
	                	<button class="btn btn-danger btn-sm delete" data-id="{{$event->id}}"><span class="oi oi-x"></span></button>
	                	<a href="eventos/editar/{{$event->id}}" class="btn btn-primary btn-sm">Editar</a>
	                	<a href="{{url('escola/eventos/aprovar')}}/{{$event->id}}" class="btn btn-success btn-sm">Moderar Fotos</a>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>

	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/event/index.js')}}"></script>
@endpush