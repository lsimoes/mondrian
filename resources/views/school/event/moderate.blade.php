@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">
		<h6 class="pt-4 pb-4">{{$event->name}}</h6>

		<div class="container">
		    <div class="row">
		    	@foreach(explode(';', $event->images) as $image)
		    	@if($image != '') 
		        <div class="col-md-4 mb-4">
		            <img class="card-img-top img-fluid" src="{{ Storage::disk('local')->url('events/') }}{{$image}}" alt="Card image cap">
		            <div class="card-body">
		                <button class="btn btn-danger btn-sm float-right delete" data-image="{{$image}}"><span class="oi oi-trash"></span></button>
		            </div>
		        </div>
		        @endif
		        @endforeach
		    </div>
		    <input type="hidden" id="event_id" name="event_id" @if(isset($event)) value="{{$event->id}}" @endif>
		    <a href="{{url('escola/evento/aprovar')}}" class="btn btn-success float-right publish">Publicar</a>
		</div>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/event/moderate.js')}}"></script>
@endpush