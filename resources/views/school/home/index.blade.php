@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		<h5 class="pt-4 pb-2">Usuários aguardando liberação</h5>

		<table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>Nome</th>
	                <th>E-mail</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($users as $user)
	            <tr>
	                <td>{{$user->name}}</td>
	                <td>{{$user->email}}</td>
	                <td>
	                	<button class="btn btn-danger btn-sm delete" data-id="{{$user->id}}"><span class="oi oi-x"></span></button>
	                	<button class="btn btn-success btn-sm approve" data-id="{{$user->id}}"><span class="oi oi-check"></span></button>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>

	    <hr />

	    <h5 class="pt-4 pb-4">Alunos aguardando liberação de matrícula</h5>

	    <table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>Nome do Aluno</th>
	                <th>Mãe/Pai</th>
	                <th>Status</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($students as $student)
	            <tr>
	                <td>{{$student->name_student}}</td>
	                <td>{{$student->father_power}}</td>
	                <td>Aguardando Matrícula</td>
	                <td>
	                	<a href="alunos/editar/{{$student->id}}" class="btn btn-primary btn-sm">Editar</a>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/home/index.js')}}"></script>
	<script type="text/javascript" src="{{url('js/school/user/index.js')}}"></script>
@endpush