@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		<a href="{{url('/escola/calendarios/incluir')}}" class="btn btn-success mt-4 float-right">Incluir</a>
		<h6 class="pt-4 pb-4">Calendário</h6>
		
		<div id='calendario'></div>

	</div>
@endsection

@push('scripts')
	<script src="{{url('js/dist/fullcalendar/packages/core/main.js')}}"></script>
    <script src="{{url('js/dist/fullcalendar/packages/daygrid/main.js')}}"></script>
	<script src="{{url('js/school/reuniao/calendario/index.js')}}"></script>
@endpush

@push('css')
    <link href="{{url('js/dist/fullcalendar/packages/core/main.css')}}" rel='stylesheet' />
    <link href="{{url('js/dist/fullcalendar/packages/daygrid/main.css')}}" rel='stylesheet' />
@endpush