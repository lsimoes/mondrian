@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">
		<h6 class="pt-4 pb-4">Novo horário de reunião</h6>

		<form id="formMeeting">
		  <div class="form-row">
		  	<div class="form-group col-md-6">
		      <label for="class_room_id">Turma</label>
		      <select class="form-control" name="class_room_id" id="class_room_id" required>
		      	<option value="">Selecione</option>
		      	@foreach($classrooms as $classroom)
		      		<option value="{{$classroom->id}}" @if(isset($meeting) && $meeting->class_room_id == $classroom->id) selected @endif>{{$classroom->name}} - {{$classroom->teacher}}</option>
		      	@endforeach
		      </select>
		    </div>
		  </div>
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="teacher">Descrição</label>
		      <textarea class="form-control" name="description" id="description" placeholder="Descrição" required>@if(isset($meeting)) {{$meeting->description}} @endif</textarea>
		    </div>
		  </div>
		  	<div class="form-row">
			    <div class="form-group col-md-12">
			  		Data
			  	</div>

			  	@if(isset($meeting))
			  		@foreach(explode(';', $meeting->dates) as $date)
					  	<div class="col-md-12">
						    <div class="form-group col-md-5">
						    	<input type="date" class="form-control" name="dates[]" value="{{$date}}" id="dates" required> 
						    </div>
						    <div class="form-group col-md-1">
						    	<a class="btn btn-success addDate"><span class="oi oi-plus"></span></a>
						    </div>
						</div>
					@endforeach
				@else
					<div class="col-md-12">
						    <div class="form-group col-md-5">
						    	<input type="date" class="form-control" name="dates[]" id="dates" required> 
						    </div>
						    <div class="form-group col-md-1">
						    	<a class="btn btn-success addDate"><span class="oi oi-plus"></span></a>
						    </div>
						</div>
				@endif
			</div>

			

		  	<div class="form-row">
			  	<div class="form-group col-md-12">
			  		Hora disponível
			  	</div>
			  	<div class="col-md-3">
				    	<!-- <input type="time" class="form-control" name="hours[]" id="hours">  -->
		    	<div class="form-check">
			    	<input class="form-check-input" type="checkbox" name="hours[]" value="07:00" id="07:00">
						<label class="form-check-label" for="07:00">07:00</label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="hours[]"  value="07:30" id="07:30">
						<label class="form-check-label" for="07:30">07:30</label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="hours[]"  value="08:00" id="08:00">
						<label class="form-check-label" for="08:00">08:00</label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="hours[]"  value="08:30" id="08:30">
						<label class="form-check-label" for="08:30">08:30</label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="hours[]"  value="09:00" id="09:00">
						<label class="form-check-label" for="09:00">09:00</label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="hours[]"  value="09:30" id="09:30">
						<label class="form-check-label" for="09:30">09:30</label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="hours[]"  value="10:00" id="10:00">
						<label class="form-check-label" for="10:00">10:00</label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="hours[]"  value="10:30" id="10:30">
						<label class="form-check-label" for="10:30">10:30</label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="hours[]"  value="11:00" id="11:00">
						<label class="form-check-label" for="11:00">11:00</label>
					</div>

					<div class="form-check">
						<input class="form-check-input" type="checkbox" name="hours[]"  value="11:30" id="11:30">
						<label class="form-check-label" for="11:30">11:30</label>
					</div>

					<div class="form-check">
			    	<input class="form-check-input" type="checkbox" name="hours[]" value="12:00" id="12:00">
						<label class="form-check-label" for="12:00">12:00</label>
					</div>

					<div class="form-check">
			    		<input class="form-check-input" type="checkbox" name="hours[]" value="12:30" id="12:30">
						<label class="form-check-label" for="12:30">12:30</label>
					</div>

					<div class="form-check">
			    		<input class="form-check-input" type="checkbox" name="hours[]" value="13:00" id="13:00">
						<label class="form-check-label" for="13:00">13:00</label>
					</div>

					
				</div>
				<div class="col-md-3">
					<div class="form-check">
			    		<input class="form-check-input" type="checkbox" name="hours[]" value="13:30" id="13:30">
						<label class="form-check-label" for="13:30">13:30</label>
					</div>

					<div class="form-check">
			    	<input class="form-check-input" type="checkbox" name="hours[]" value="14:00" id="14:00">
						<label class="form-check-label" for="14:00">14:00</label>
					</div>

					<div class="form-check">
			    	<input class="form-check-input" type="checkbox" name="hours[]" value="14:30" id="14:30">
						<label class="form-check-label" for="14:30">14:30</label>
					</div>

					<div class="form-check">
			    	<input class="form-check-input" type="checkbox" name="hours[]" value="15:00" id="15:00">
						<label class="form-check-label" for="15:00">15:00</label>
					</div>

					<div class="form-check">
			    	<input class="form-check-input" type="checkbox" name="hours[]" value="15:30" id="15:30">
						<label class="form-check-label" for="15:30">15:30</label>
					</div>

					<div class="form-check">
			    	<input class="form-check-input" type="checkbox" name="hours[]" value="16:00" id="16:00">
						<label class="form-check-label" for="16:00">16:00</label>
					</div>

					<div class="form-check">
			    	<input class="form-check-input" type="checkbox" name="hours[]" value="16:30" id="16:30">
						<label class="form-check-label" for="16:30">16:30</label>
					</div>

					<div class="form-check">
			    	<input class="form-check-input" type="checkbox" name="hours[]" value="17:00" id="17:00">
						<label class="form-check-label" for="17:00">17:00</label>
					</div>

					<div class="form-check">
			    	<input class="form-check-input" type="checkbox" name="hours[]" value="17:30" id="17:30">
						<label class="form-check-label" for="17:30">17:30</label>
					</div>

					<div class="form-check">
			    	<input class="form-check-input" type="checkbox" name="hours[]" value="18:00" id="18:00">
						<label class="form-check-label" for="18:00">18:00</label>
					</div>

					<div class="form-check">
			    	<input class="form-check-input" type="checkbox" name="hours[]" value="18:30" id="18:30">
						<label class="form-check-label" for="18:30">18:30</label>
					</div>
				</div>

		  	</div>
		  	<input type="hidden" id="meeting_id" name="meeting_id" @if(isset($meeting)) value="{{$meeting->id}}" @endif>
		  	@if(isset($meeting))
		  		<button class="btn btn-danger float-left delete mt-4" data-id='{{$meeting->id}}'>Excluir</button>
		  	@endif
		  <button id="btnNew" type="submit" class="btn btn-success float-right mt-4 mb-4">Salvar</button>
		</form>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/meeting/create-edit.js')}}"></script>
@endpush