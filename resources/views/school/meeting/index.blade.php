@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		<a href="{{url('/escola/reunioes/incluir')}}" class="btn btn-success mt-4 float-right">Incluir</a>

		<a href="{{url('/escola/reunioes/calendario')}}" class="btn btn-info mt-4 mr-2 float-right">Ver calendário</a> 

		<h5 class="pt-4 pb-4">Reuniões confirmadas</h5>
		

		<table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>Professora</th>
	                <th>Turma</th>
	                <th>Aluno</th>
	                <th>Confirmado por</th>
	                <th>Data</th>
	                <th>Hora</th>
	                <th>Ações</th>
	                <!-- <th></th> -->
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($arrMeetings as $meeting)
	            <tr>
	                <td>{{$meeting['meetingTeacher']}}</td>
	                <td>{{$meeting['meetingClassRoom']}}</td>
	                <td>{{$meeting['meetingStudent']}}</td>
	                <td>{{$meeting['userName']}}</td>
	                <td>{{ Carbon\Carbon::parse($meeting['meetingDate'])->format('d/m/Y') }}</td>
	                <td>{{$meeting['meetingHour']}}</td>
	                <td>
	                	<button class="btn btn-danger btn-sm delete" data-confirmation="{{$meeting['userId']}},{{$meeting['meetingDate']}},{{$meeting['meetingHour']}},{{$meeting['studentId']}},{{$meeting['meetingId']}}"><span class="oi oi-x"></span></button>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>

	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/meeting/index.js')}}"></script>
@endpush