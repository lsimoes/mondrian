@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">
		<h6 class="pt-4 pb-4">Novo Aviso</h6>

		<form id="formNotice">
		  <div class="form-row">
		    <div class="form-group col-md-12">
		      <label for="subject">Assunto</label>
		      <input type="text" class="form-control" name="subject" id="subject" placeholder="Assunto" @if(isset($notice)) value="{{$notice->subject}}" @endif required>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="date">Data</label>
		      <input type="date" class="form-control" name="date" id="date" placeholder="Data" @if(isset($notice)) value="{{$notice->date}}" @endif required>
		    </div>
		    <div class="form-group col-md-6">
		      	<label for="type">Tipo </label>
		      	<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="type" id="type1" value="1" @if(isset($notice) && $notice->type == 1) checked  @endif required>
				  <label class="form-check-label" for="type1">Circular</label>
				</div>
				<div class="form-check form-check-inline">
				  <input class="form-check-input" type="radio" name="type" id="type2" value="2" @if(isset($notice) && $notice->type == 2) checked  @endif required>
				  <label class="form-check-label" for="type2">Comunicado</label>
				</div>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="class_room_id">Turma</label>
		      <select class="form-control" name="class_room_id[]" id="class_room_id" multiple required>
		      	
		      	@foreach($classrooms as $classroom)
		      		<option value="{{$classroom->id}}" @if(isset($notice) && $notice->class_room_id == $classroom->id) selected @endif>{{$classroom->name}}</option>
		      	@endforeach
		      </select>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="number">Número</label>
		      <input type="text" class="form-control" name="number" id="number" placeholder="Número" @if(isset($notice)) value="{{$notice->number}}" @endif required>
		    </div>
		  </div>
		  <div class="form-row">
		    <div class="form-group col-md-12">
		    	<label for="notice">Aviso</label>
		    	<textarea class="form-control summernote" name="notice" id="notice" required>@if(isset($notice)) {{$notice->notice}} @endif</textarea>
		    </div>
		  </div>
		  <input type="hidden" id="notice_id" name="notice_id" @if(isset($notice)) value="{{$notice->id}}" @endif>
		  <button id="btnNew" type="submit" class="btn btn-success float-right">
		  		Salvar
		  </button>
		</form>
	</div>
@endsection

@push('scripts')
	<script src="{{url('js/summernote/summernote-bs4.js')}}"></script>
	<script type="text/javascript" src="{{url('js/school/notice/create-edit.js')}}"></script>
@endpush

@push('css')
    <link href="{{url('js/summernote/summernote-bs4.css')}}" rel="stylesheet"/>
@endpush