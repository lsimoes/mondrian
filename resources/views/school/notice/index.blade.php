@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		<a href="{{url('/escola/avisos/incluir')}}" class="btn btn-success mt-4 float-right">Incluir</a> 

		<h5 class="pt-4 pb-4">Avisos</h5>
		

		<table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>Assunto</th>
	                <th>Turma</th>
	                <th>Circular N.</th>
	                <th>Data</th>
	                <th>Cientes</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($noticesArr as $notice)
	            <tr>
	                <td>{{$notice['noticeSubject']}}</td>
	                <td>{{$notice['classRoomName']}}</td>
	                <td>{{$notice['noticeNumber']}}</td>
	                <td>{{ Carbon\Carbon::parse($notice['noticeDate'])->format('d/m/Y') }}</td>
	                <td><button class="btn btn-success awares" data-awares="<ul>@foreach($notice['awares'] as $aware) <li>{{$aware}}</li> @endforeach <ul>">Visualizar</button></td>
	                <td>
	                	<button class="btn btn-danger btn-sm delete" data-id="{{$notice['noticeId']}}"><span class="oi oi-x"></span></button>
	                <a href="/escola/avisos/editar/{{$notice['noticeId']}}" class="btn btn-primary btn-sm">Editar</a>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>

	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/notice/index.js')}}"></script>
@endpush