@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		<!-- <a href="{{url('/escola/alunos/incluir')}}" class="btn btn-success mt-4 float-right">Incluir</a>  -->

		<h5 class="pt-4 pb-4">Alunos</h5>
		

		<table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>Nome</th>
	                <th>Turma</th>
	                <th>Responsável</th>
	                <th>Status</th>
	                <th>Última alteração</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($students as $student)
	            <tr>
	                <td>{{$student->studentName}}</td>
	                <td>{{$student->classRoomName}}</td>
	                <td>{{$student->userName}} - {{$student->userEmail}}</td>
	                <td>
	                	@if($student->studentStatus == 2)
	                		Aguardando matrícula
	                	@else
	                		Matriculado {{$student->studentMatriculationDate}}
	                	@endif
	                </td>
	                <td>{{ Carbon\Carbon::parse($student->studentUpdatedAt)->format('d/m/Y H:m') }}</td>
	                <td>
	                	<a href="/escola/alunos/editar/{{$student->studentId}}" class="btn btn-primary btn-sm">Editar</a>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>

	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/student/index.js')}}"></script>
@endpush