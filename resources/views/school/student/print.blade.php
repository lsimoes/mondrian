@extends('school.templates.template2')

@section('content')

	<style>
		body {font-size: 18px !important;}
		label {font-weight: bold; margin-bottom: -3px !important;}
		.form-group {margin-bottom: -8px !important;}
	</style>

	

	<div class="col-lg-12 mx-auto " id="print">	
		<img src="/storage/watermark.png">

		<div class="form-row pt-2 mt-4 mb-4 bg-light" style="border-radius: 5px;">

			<div class="form-group col-md-3">
		      <img src="{{ Storage::disk('local')->url('students/') }}{{$student->photo}}" width="200" />
		    </div>

		    
		    <div class="form-group col-md-9">

			    	<div class="row">
				      	<div class="form-group col-md-12">
					      <h5>{{$student->name_student}}</h5>
					    </div>

					    <div class="form-group col-md-6">
					    	Status:
					    	@if($student->status == null)
		                		<strong>Aguardando matrícula</strong>
		                	@else
		                		<strong>Matriculado {{$student->matriculation_date}}</strong>
		                	@endif
					    </div>
					    <div class="form-group col-md-6">
					    	Reponsável pelas informações cadastradas<br>
					    	<strong>{{$parent->name}}</strong> - <strong>{{$parent->email}}</strong>
					    </div>
					    <div class="form-group col-md-3 mt-2" style="color: #da2818; font-size: 20px;">
					      <label for="ri">RI: </label>
					      {{$student->rinumber}}
					    </div>
					    <div class="form-group col-md-3 mt-2" style="color: #da2818; font-size: 20px;">
					      <label for="matriculation_date">Ano Letivo: </label>
					      {{$student->matriculation_date}}
					    </div>
					    <div class="form-group col-md-6 mt-2" style="color: #da2818; font-size: 20px;">
					    	<label for="blood_group">Turma: </label>
					    	@if(isset($class_room->name))
					      		{{$class_room->name}}
					      	@endif
					    </div>
					    
					</div>
			    </div>

		</div>

	

			<div class="form-row">
			    <div class="form-group col-md-6">
			      <label for="name_student">Nome Completo: </label>
			      {{$student->name_student}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="date_birth_student">Data de Nascimento: </label>
			      {{ Carbon\Carbon::parse($student->date_birth_student)->format('d/m/Y') }}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="date_birth_student">Raça: </label>
			      {{$student->race}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="father_power">A criança está sob o pátrio poder do(a):</label>
			      {{$student->father_power}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="catch_child">Quem, habitualmente, poderá retirar a criança:</label>
			      {{$student->catch_child}}
			    </div>
			    
			</div>
			<div class="form-row">

			    <div class="form-group col-md-12">
			      <label for="name_student">Período: </label>

			      @if($student->period == 'm')
			    	Manhã
				    @endif

				    @if($student->period == 't')
				    	Tarde
				    @endif

				    @if($student->period == 'int')
				    	Integral
				    @endif

				    @if($student->period == 'ind')
						Intermediário - entrada às 10:00h
				    @endif

					@if($student->period == 'ind2')
						Intermediário - saída às 14:30h
				    @endif
			    </div>
  
			</div>

			<div class="form-row">

			    <div class="form-group col-md-12">
			      <label for="name_student">Refeição: </label>
					@foreach($meals as $meal)
				  		@if($meal == 'a')
				  			Almoço &nbsp;&nbsp;/
				  		@endif
				  		@if($meal == 'j')
				  			Jantar &nbsp;&nbsp;/
				  		@endif
			  		@endforeach
			    </div>
  
			</div>


			<div class="form-row">

			    <div class="form-group col-md-12">
			      @foreach($brothers as $brother)
				  		<label for="inputPassword4">Irmão(ã): &nbsp;</label> {{$brother}}   &nbsp;&nbsp;/&nbsp;&nbsp;   
			  		@endforeach
			    </div>
  
			</div>

		  	
		  	
		<hr/>  

			<div class="form-row mt-4">
			    <div class="form-group col-md-6">
			      <label for="allergy">Possui alguma alergia: </label>
			      {{$student->allergy}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="food_restriction">Possui alguma Restrição Alimentar: </label>
			      {{$student->food_restriction}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="medicines_often">Toma algum medicamentos frequentemente: </label>
			      {{$student->medicines_often}}
			    </div>
			    <div class="form-group col-md-6">
			    	<label for="blood_group">Grupo Sanguíneo: </label>
			      	{{$student->blood_group}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="medical_agreement">Possui Convênio Médico: Qual: </label>
			      {{$student->medical_agreement}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="fever_case">Em caso de febre: </label>
			      {{$student->fever_case}}
			    </div>
			    <div class="form-group col-md-12">
			      <label for="health_observations">Outras observações de saúde: </label>
			      {{$student->health_observations}}
			    </div>
			</div>
			<input type="hidden" class="student_id" name="student_id">


		<hr/>

	  		<div class="form-row mt-4">
				<div class="form-group col-md-12">
					<h5><strong>ENDEREÇO DO ALUNO</strong></h5>
				</div>
			    <div class="form-group col-md-6">
			      <label for="cep_student">CEP: </label>
			      {{$student->cep_student}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="address_student">Endereço: </label>
			      {{$student->address_student}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="number_student">Número: </label>
			      {{$student->number_student}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="complement_student">Complemento: </label>
			      {{$student->complement_student}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="neighborhood_student">Bairro: </label>
			      {{$student->neighborhood_student}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="city_student">Cidade: </label>
			      {{$student->city_student}}
			    </div>
			    <div class="form-group col-md-6">
			      	<label for="state_student">Estado</label>
			      	{{$student->state_student}}
			    </div>
			</div>


		<hr/>

			<div class="form-row mt-4">
				<div class="form-group col-md-12">
					<h5><strong>FILIAÇÃO 1</strong></h5>
				</div>
			    <div class="form-group col-md-6">
			      <label for="name_dad">Nome: </label>
			      {{$student->name_dad}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="date_birth_dad">Data de Nascimento: </label>
			      {{$student->date_birth_dad}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="cpf_dad">CPF: </label>
			      {{$student->cpf_dad}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="email_dad">E-mail: </label>
			      {{$student->email_dad}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="cell_dad">Celular: </label>
			      {{$student->cell_dad}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="job_dad">Profissão: </label>
			      {{$student->job_dad}}
			    </div>
			</div>

			<div class="form-row mt-2">
				<div class="form-group col-md-12">
					<h5><strong>Endereço Comercial</strong></h5>
				</div>
			    <div class="form-group col-md-6">
			      <label for="comm_tel_dad">Telefone Comercial: </label>
			      {{$student->comm_tel_dad}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_cep_dad">CEP: </label>
			      {{$student->comm_cep_dad}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_address_dad">Endereço: </label>
			      {{$student->comm_address_dad}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_number_dad">Número: </label>
			      {{$student->comm_number_dad}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_complement_dad">Complemento: </label>
			      {{$student->comm_complement_dad}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_neighborhood_dad">Bairro: </label>
			      {{$student->comm_neighborhood_dad}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_city_dad">Cidade: </label>
			      {{$student->comm_city_dad}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_state_dad">Estado</label>
			      {{$student->comm_state_dad}}
			    </div>
			</div>

			<hr />

			<div class="form-row mt-4">
				<div class="form-group col-md-12">
					<h5><strong>FILIAÇÃO 2</strong></h5>
				</div>
			    <div class="form-group col-md-6">
			      <label for="name_mom">Nome: </label>
			      {{$student->name_mom}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="date_birth_mom">Data de Nascimento: </label>
			      {{$student->date_birth_mom}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="cpf_mom">CPF: </label>
			      {{$student->cpf_mom}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="email_mom">E-mail: </label>
			      {{$student->email_mom}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="cell_mom">Celular: </label>
			      {{$student->cell_mom}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="job_mom">Profissão: </label>
			      {{$student->job_mom}}
			    </div>
			</div>

			<div class="form-row mt-2">
				<div class="form-group col-md-12">
					<h5><strong>Endereço Comercial</strong></h5>
				</div>
			    <div class="form-group col-md-6">
			      <label for="comm_tel_mom">Telefone Comercial: </label>
			      {{$student->comm_tel_mom}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_cep_mom">CEP: </label>
			      {{$student->comm_cep_mom}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_address_mom">Endereço: </label>
			      {{$student->comm_address_mom}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_number_mom">Número: </label>
			      {{$student->comm_number_mom}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_complement_mom">Complemento: </label>
			      {{$student->comm_complement_mom}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_neighborhood_mom">Bairro: </label>
			      {{$student->comm_neighborhood_mom}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_city_mom">Cidade: </label>
			      {{$student->comm_city_mom}}
			    </div>
			    <div class="form-group col-md-6">
			      <label for="comm_state_mom">Estado</label>
			      {{$student->comm_state_mom}}
			    </div>
			</div>


		<hr/>


	  		<h4 class="mt-2">AUTORIZAÇÃO DE USO DE IMAGEM</h4>
	  		<p>
	  			A Recreação Infantil Mondrian, livre de quaisquer ônus para com o aluno(a) e seu(sua) responsável, solicita a autorização para utilizar-se da sua imagem para fins exclusivos de divulgação da escola e suas atividades não publicitárias. Podendo, para tanto, reproduzi-la ou divulga-la junto a Internet, no site, redes sociais abertas, revistas, jornais e todos os demais meios de comunicação, público ou privado. 
	  		</p>
	  		<p>
	  			<strong>
		  			@if($student->image_authorization == 1)
		  				Autorizo a utilização da imagem durante o ano letivo, conforme previsto nos termos acima.
		  			@endif

		  			@if($student->image_authorization == 2)
		  				Autorizo sua participação apenas nas fotos da rotina, expostas dentro da escola, no ambiente restrito das redes sociais e do site da escola e nas imagens coladas nas apostilas dos alunos.
		  			@endif

		  			@if($student->image_authorization == 3)
		  				Não autorizo a utilização de imagem de maneira nenhuma.
		  			@endif
	  			</strong>
	  		</p>

		
	</div>
		
	</div>
@endsection

@push('scripts')
	
	<script type="text/javascript">
	$(document).ready(function(){
		window.print();
	});
	</script>
@endpush