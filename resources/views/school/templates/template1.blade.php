<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Mondrian - {{$title}}</title>

  <link rel="stylesheet" type="text/css" href="{{url('css/styles.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{url('js/dist/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('js/dist/datatables/datatables.min.css')}}"/>
  <link rel="stylesheet" href="{{url('js/dist/open-iconic/font/css/open-iconic-bootstrap.css')}}"/>

  @stack('css')

</head>

<body>

  <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
    <div class="container">
      <a class="navbar-brand" href="{{url('/escola/home')}}"><img src="{{ Storage::disk('local')->url('/') }}watermark.png"></a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item @if (Request::segment(2) == 'banners') active @endif">
            <a class="nav-link" href="{{url('/escola/banners')}}">Banner Interno
            </a>
          </li>
          <li class="nav-item dropdown @if (Request::segment(2) == 'calendarios') active @endif">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Calendário
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="{{url('/escola/calendarios')}}">Calendário</a>
              <a class="dropdown-item" href="{{url('/escola/calendarios/categorias')}}">Categorias</a>
            </div>
          </li>
          <li class="nav-item @if (Request::segment(2) == 'reunioes') active @endif">
            <a class="nav-link" href="{{url('/escola/reunioes')}}">Reuniões</a>
          </li>
          <li class="nav-item @if (Request::segment(2) == 'eventos') active @endif">
            <a class="nav-link" href="{{url('/escola/eventos')}}">Eventos</a>
          </li>
          <li class="nav-item @if (Request::segment(2) == 'turmas') active @endif">
            <a class="nav-link" href="{{url('/escola/turmas')}}">Turmas</a>
          </li>
          <li class="nav-item @if (Request::segment(2) == 'usuarios') active @endif">
            <a class="nav-link" href="{{url('/escola/usuarios')}}">Usuários</a>
          </li>
          <li class="nav-item @if (Request::segment(2) == 'alunos') active @endif">
            <a class="nav-link" href="{{url('/escola/alunos')}}">Alunos</a>
          </li>
          <li class="nav-item @if (Request::segment(2) == 'avisos') active @endif">
            <a class="nav-link" href="{{url('/escola/avisos')}}">Avisos</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="{{url('/sair')}}">Sair</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
          <div class="col-lg-12 mt-1">
            <a href="/">Home</a> >                
            <?php $link = "" ?>
            @for($i = 1; $i <= count(Request::segments()); $i++)
                @if($i < count(Request::segments()) & $i > 0)
                <?php $link .= "/" . Request::segment($i); ?>
                <a href="<?= $link ?>">{{ ucwords(str_replace('-',' ',Request::segment($i)))}}</a> >
                @else {{ucwords(str_replace('-',' ',Request::segment($i)))}}
                @endif
            @endfor
            <a href="javascript:window.location = document.referrer;" class="float-right">< Voltar</a>
          </div>
        @yield('content')
      </div>
    </div>
  </div>

  <div class="modal" id="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="{{url('js/dist/jquery/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/dist/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/dist/datatables/datatables.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/dist/datatables/date-euro.js')}}"></script>
  @stack('scripts')

</body>

</html>
