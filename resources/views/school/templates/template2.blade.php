<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <title>Mondrian - {{$title}}</title>

  <link rel="stylesheet" type="text/css" href="{{url('css/styles.css')}}"/>
  <link rel="stylesheet" type="text/css" href="{{url('js/dist/bootstrap/css/bootstrap.min.css')}}">
  <link rel="stylesheet" type="text/css" href="{{url('js/dist/datatables/datatables.min.css')}}"/>
  <link rel="stylesheet" href="{{url('js/dist/open-iconic/font/css/open-iconic-bootstrap.css')}}"/>

  @stack('css')

</head>

<body>

  

  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        @yield('content')
      </div>
    </div>
  </div>

  <div class="modal" id="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title"></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript -->
  <script type="text/javascript" src="{{url('js/dist/jquery/jquery.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/dist/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
  <script type="text/javascript" src="{{url('js/dist/datatables/datatables.min.js')}}"></script>
  @stack('scripts')

</body>

</html>
