@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">
		<h6 class="pt-4 pb-4">Usuário</h6>

		<form id="formUser">
		  <div class="form-row">
		    <div class="form-group col-md-6">
		      <label for="name">Nome</label>
		      <input type="text" class="form-control" name="name" id="name" placeholder="Nome" @if(isset($user)) value="{{$user->name}}" @endif required>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="email">E-mail</label>
		      <input type="email" class="form-control" name="email" id="email" placeholder="E-mail" @if(isset($user)) value="{{$user->email}}" @endif required>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="password">Senha</label>
		      <input type="password" class="form-control" name="password" id="password" placeholder="Senha" required>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="confirm_password">Confirmar Senha</label>
		      <input type="password" class="form-control" name="confirm_password" id="confirm_password" placeholder="Confirmar Senha" required>
		    </div>
		    <div class="form-group col-md-6">
		      <label for="isAdmin">Admin</label>
		      <input value="1" type="checkbox" name="isAdmin" id="isAdmin" @if(isset($user) && $user->isAdmin != null) checked @endif>
		    </div>
		  </div>
		  <input type="hidden" id="user_id" name="user_id" @if(isset($user)) value="{{$user->id}}" @endif>
		  <button id="btnNew" type="submit" class="btn btn-success float-right">Salvar</button>
		</form>
	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/user/create-edit.js')}}"></script>
@endpush