@extends('school.templates.template1')

@section('content')

	
	<div class="col-lg-12 mx-auto">

		<a href="{{url('/escola/usuarios/incluir')}}" class="btn btn-success mt-4 float-right">Incluir</a> 

		<h5 class="pt-4 pb-4">Usuários</h5>
		

		<table class="table table-striped table-bordered" style="width:100%">
	        <thead>
	            <tr>
	                <th>Nome</th>
	                <th>E-mail</th>
	                <th>Permissão</th>
	                <th></th>
	            </tr>
	        </thead>
	        <tbody>
	        	@foreach($users as $user)
	            <tr>
	                <td>{{$user->name}}</td>
	                <td>{{$user->email}}</td>
	                <td>
	                	@if($user->isAdmin == 1)
	                		Administrador
	                	@else
	                		Pai/Mãe
	                	@endif
	                </td>
	                <td>
	                	<button class="btn btn-danger btn-sm delete" data-id="{{$user->id}}"><span class="oi oi-x"></span></button>
	                	<a href="/escola/usuarios/editar/{{$user->id}}" class="btn btn-primary btn-sm">Editar</a>
	                </td>
	            </tr>
	            @endforeach
	        </tbody>
	    </table>

	</div>
@endsection

@push('scripts')
	<script type="text/javascript" src="{{url('js/school/user/index.js')}}"></script>
@endpush