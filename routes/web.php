<?php

    /* ### Escola ### */

	//Dashboard
    Route::get('/escola/home','HomeSchoolController@index');
    Route::get('/escola','HomeSchoolController@index');

    //Banner
    Route::get('/escola/banners','BannerSchoolController@index');
    Route::get('/escola/banners/incluir','BannerSchoolController@incluir');
    Route::get('/escola/banners/editar/{id}','BannerSchoolController@edit');
        //Api
        Route::post('/api/school/insert/banner','BannerSchoolController@insert');
        Route::post('/api/school/delete/banner','BannerSchoolController@delete');
        Route::post('/api/school/update/banner','BannerSchoolController@update');

    //Calendario
    Route::get('/escola/calendarios','CalendarSchoolController@index');
    Route::get('/escola/calendarios/incluir','CalendarSchoolController@incluir');
    Route::get('/escola/calendarios/editar/{id}','CalendarSchoolController@edit');
        //Api
        Route::get('/api/school/calendar/load','CalendarSchoolController@load');
        Route::post('/api/school/insert/calendar','CalendarSchoolController@insertCalendar');
        Route::post('/api/school/update/calendar','CalendarSchoolController@update');
        Route::post('/api/school/delete/calendar','CalendarSchoolController@delete');

    Route::get('/escola/calendarios/categorias','CalendarCategorySchoolController@index');
    Route::get('/escola/calendarios/categorias/adicionar','CalendarCategorySchoolController@adicionar');
    Route::get('/escola/calendarios/categorias/editar/{id}','CalendarCategorySchoolController@edit');
        //API
        Route::post('/api/school/insert/calendar/category','CalendarCategorySchoolController@insert');
        Route::post('/api/school/delete/calendar/category','CalendarCategorySchoolController@delete');
        Route::post('/api/school/update/calendar/category','CalendarCategorySchoolController@update');
    

    //Reunioes
    Route::get('/escola/reunioes','MeetingSchoolController@index');
    Route::get('/escola/reunioes/incluir','MeetingSchoolController@incluir');
    Route::get('/escola/reunioes/calendario','MeetingSchoolController@calendario');
    Route::get('/escola/reunioes/editar/{id}','MeetingSchoolController@edit');
        //Api
        Route::post('/api/school/insert/meeting','MeetingSchoolController@insert');
        Route::post('/api/school/update/meeting','MeetingSchoolController@update');
        Route::get('/api/school/load/meeting','MeetingSchoolController@load');
        Route::post('/api/school/delete/meeting','MeetingSchoolController@delete');
        Route::post('/api/school/delete/meeting/confirmation','MeetingSchoolController@deleteConfirmation');

    //Eventos
    Route::get('/escola/eventos','EventSchoolController@index');
    Route::get('/escola/eventos/incluir','EventSchoolController@incluir');
    Route::get('/escola/eventos/aprovar/{id}','EventSchoolController@aprovar');
    Route::get('/escola/eventos/editar/{id}','EventSchoolController@edit');
        //Api
        Route::post('/api/school/insert/event','EventSchoolController@insert');
        Route::post('/api/school/delete/event','EventSchoolController@delete');
        Route::post('/api/school/delete/event/photo','EventSchoolController@deletePhoto');
        Route::post('/api/school/publish/event','EventSchoolController@publish');
        Route::post('/api/school/update/event','EventSchoolController@update');
        
    

    //Turmas
    Route::get('/escola/turmas','ClassRoomSchoolController@index');
    Route::get('/escola/turmas/incluir','ClassRoomSchoolController@incluir');
    Route::get('/escola/turmas/editar/{id}','ClassRoomSchoolController@edit');
        //Api
        Route::post('/api/school/insert/classroom','ClassRoomSchoolController@insert');
        Route::post('/api/school/update/classroom','ClassRoomSchoolController@update');
        Route::post('/api/school/delete/classroom','ClassRoomSchoolController@delete');

    //Usuarios
    Route::get('/escola/usuarios','UserSchoolController@index');
    Route::get('/escola/usuarios/incluir','UserSchoolController@incluir');
    Route::get('/escola/usuarios/editar/{id}','UserSchoolController@edit');
        //Api
        Route::post('/api/school/insert/user','UserSchoolController@insert');
        Route::post('/api/school/update/user','UserSchoolController@update');
        Route::post('/api/school/delete/user','UserSchoolController@delete');
        Route::post('/api/school/approve/user','UserSchoolController@approve');

    //Alunos
    Route::get('/escola/alunos','StudentSchoolController@index');
    Route::get('/escola/alunos/editar/{id}','StudentSchoolController@edit');
    Route::get('/escola/alunos/imprimir/{id}','StudentSchoolController@print');
        //Api
        Route::post('/api/school/update/student','StudentSchoolController@update');
        Route::post('/api/school/cancel/student','StudentSchoolController@cancel');
        Route::post('/api/school/delete/student','StudentSchoolController@delete');


    //Avisos
    Route::get('/escola/avisos','NoticeSchoolController@index');
    Route::get('/escola/avisos/incluir','NoticeSchoolController@incluir');
    Route::get('/escola/avisos/editar/{id}','NoticeSchoolController@edit');
        //Api
        Route::post('/api/school/insert/notice','NoticeSchoolController@insert');
        Route::post('/api/school/update/notice','NoticeSchoolController@update');
        Route::post('/api/school/delete/notice','NoticeSchoolController@delete');


    /* ### Pais ### */

    //Dashboard
    Route::get('/pais/home','HomeParentController@index');
    Route::get('/pais','HomeParentController@index');

    //Avisos
    Route::get('/pais/avisos','NoticeParentController@notices');
    Route::get('/pais/avisos/{id}','NoticeParentController@notice');
        //API
        Route::post('/api/parent/aware/notice','NoticeParentController@aware');

    //Reunioes
    Route::get('/pais/reunioes','MeetingParentController@index');
    Route::get('/pais/reunioes/agendar/{id}/{studentId}','MeetingParentController@schedule');
        //Api
        Route::post('/api/parent/confirm/meeting','MeetingParentController@confirm');

    //Eventos
    Route::get('/pais/eventos','EventParentController@index');
    Route::get('/pais/eventos/{id}','EventParentController@event');

    //Calendario
    Route::get('/pais/calendario','CalendarParentController@index');
    Route::get('/pais/calendario/{id}','CalendarParentController@view');
        //API
        Route::get('/api/parent/calendar/load','CalendarParentController@load');

    //Gestao de Aluno
    Route::get('/pais/alunos','StudentParentController@index');
    Route::get('/pais/alunos/matricular','StudentParentController@insert');
    Route::get('/pais/alunos/editar/{id}','StudentParentController@edit');
    //Api
        Route::post('/api/parent/insert/student/step1','StudentParentController@step1');
        Route::post('/api/parent/insert/student/step2','StudentParentController@step2');
        Route::post('/api/parent/insert/student/step3','StudentParentController@step3');
        Route::post('/api/parent/insert/student/step4','StudentParentController@step4');
        Route::post('/api/parent/insert/student/step5','StudentParentController@step5');
        Route::post('/api/parent/delete/student','StudentParentController@delete');
        Route::post('/api/parent/rematch/student','StudentParentController@rematch');

    //Configurações
    Route::get('/pais/configuracoes','UserParentController@index');
        //API
        Route::post('/api/parent/update/user','UserParentController@update');


        

    Auth::routes();
    Route::get('/','HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/sair', 'HomeController@logout');
